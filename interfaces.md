# Interfaces

## <a name="actor"></a>Actor

> Represents an object which can take actions on GitHub. Typically a User or Bot.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `avatarUrl` | [`URI!`](scalars.md#uri) | A URL pointing to the actor's public avatar. |
| `login` | [`String!`](scalars.md#string) | The username of the actor. |
| `resourcePath` | [`URI!`](scalars.md#uri) | The HTTP path for this actor. |
| `url` | [`URI!`](scalars.md#uri) | The HTTP URL for this actor. |

### Implemented by

* `Bot`
* `EnterpriseUserAccount`
* `Mannequin`
* `Organization`
* `User`

## <a name="assignable"></a>Assignable

> An object that can have users assigned to it.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `assignees` | [`UserConnection!`](objects.md#userconnection) | A list of Users assigned to this object. |

### Implemented by

* `Issue`
* `PullRequest`

## <a name="auditentry"></a>AuditEntry

> An entry in the audit log.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `action` | [`String!`](scalars.md#string) | The action name |
| `actor` | [`AuditEntryActor`](unions.md#auditentryactor) | The user who initiated the action |
| `actorIp` | [`String`](scalars.md#string) | The IP address of the actor |
| `actorLocation` | [`ActorLocation`](objects.md#actorlocation) | A readable representation of the actor's location |
| `actorLogin` | [`String`](scalars.md#string) | The username of the user who initiated the action |
| `actorResourcePath` | [`URI`](scalars.md#uri) | The HTTP path for the actor. |
| `actorUrl` | [`URI`](scalars.md#uri) | The HTTP URL for the actor. |
| `createdAt` | [`PreciseDateTime!`](scalars.md#precisedatetime) | The time the action was initiated |
| `operationType` | [`OperationType`](enums.md#operationtype) | The corresponding operation type for the action |
| `user` | [`User`](objects.md#user) | The user affected by the action |
| `userLogin` | [`String`](scalars.md#string) | For actions involving two users, the actor is the initiator and the user is the affected user. |
| `userResourcePath` | [`URI`](scalars.md#uri) | The HTTP path for the user. |
| `userUrl` | [`URI`](scalars.md#uri) | The HTTP URL for the user. |

### Implemented by

* `MembersCanDeleteReposClearAuditEntry`
* `MembersCanDeleteReposDisableAuditEntry`
* `MembersCanDeleteReposEnableAuditEntry`
* `OauthApplicationCreateAuditEntry`
* `OrgAddBillingManagerAuditEntry`
* `OrgAddMemberAuditEntry`
* `OrgBlockUserAuditEntry`
* `OrgConfigDisableCollaboratorsOnlyAuditEntry`
* `OrgConfigEnableCollaboratorsOnlyAuditEntry`
* `OrgCreateAuditEntry`
* `OrgDisableOauthAppRestrictionsAuditEntry`
* `OrgDisableSamlAuditEntry`
* `OrgDisableTwoFactorRequirementAuditEntry`
* `OrgEnableOauthAppRestrictionsAuditEntry`
* `OrgEnableSamlAuditEntry`
* `OrgEnableTwoFactorRequirementAuditEntry`
* `OrgInviteMemberAuditEntry`
* `OrgInviteToBusinessAuditEntry`
* `OrgOauthAppAccessApprovedAuditEntry`
* `OrgOauthAppAccessDeniedAuditEntry`
* `OrgOauthAppAccessRequestedAuditEntry`
* `OrgRemoveBillingManagerAuditEntry`
* `OrgRemoveMemberAuditEntry`
* `OrgRemoveOutsideCollaboratorAuditEntry`
* `OrgRestoreMemberAuditEntry`
* `OrgUnblockUserAuditEntry`
* `OrgUpdateDefaultRepositoryPermissionAuditEntry`
* `OrgUpdateMemberAuditEntry`
* `OrgUpdateMemberRepositoryCreationPermissionAuditEntry`
* `OrgUpdateMemberRepositoryInvitationPermissionAuditEntry`
* `PrivateRepositoryForkingDisableAuditEntry`
* `PrivateRepositoryForkingEnableAuditEntry`
* `RepoAccessAuditEntry`
* `RepoAddMemberAuditEntry`
* `RepoAddTopicAuditEntry`
* `RepoArchivedAuditEntry`
* `RepoChangeMergeSettingAuditEntry`
* `RepoConfigDisableAnonymousGitAccessAuditEntry`
* `RepoConfigDisableCollaboratorsOnlyAuditEntry`
* `RepoConfigDisableContributorsOnlyAuditEntry`
* `RepoConfigDisableSockpuppetDisallowedAuditEntry`
* `RepoConfigEnableAnonymousGitAccessAuditEntry`
* `RepoConfigEnableCollaboratorsOnlyAuditEntry`
* `RepoConfigEnableContributorsOnlyAuditEntry`
* `RepoConfigEnableSockpuppetDisallowedAuditEntry`
* `RepoConfigLockAnonymousGitAccessAuditEntry`
* `RepoConfigUnlockAnonymousGitAccessAuditEntry`
* `RepoCreateAuditEntry`
* `RepoDestroyAuditEntry`
* `RepoRemoveMemberAuditEntry`
* `RepoRemoveTopicAuditEntry`
* `RepositoryVisibilityChangeDisableAuditEntry`
* `RepositoryVisibilityChangeEnableAuditEntry`
* `TeamAddMemberAuditEntry`
* `TeamAddRepositoryAuditEntry`
* `TeamChangeParentTeamAuditEntry`
* `TeamRemoveMemberAuditEntry`
* `TeamRemoveRepositoryAuditEntry`

## <a name="closable"></a>Closable

> An object that can be closed

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `closed` | [`Boolean!`](scalars.md#boolean) | `true` if the object is closed (definition of closed may depend on type) |
| `closedAt` | [`DateTime`](scalars.md#datetime) | Identifies the date and time when the object was closed. |

### Implemented by

* `Issue`
* `Milestone`
* `Project`
* `PullRequest`

## <a name="comment"></a>Comment

> Represents a comment.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `author` | [`Actor`](interfaces.md#actor) | The actor who authored the comment. |
| `authorAssociation` | [`CommentAuthorAssociation!`](enums.md#commentauthorassociation) | Author's association with the subject of the comment. |
| `body` | [`String!`](scalars.md#string) | The body as Markdown. |
| `bodyHTML` | [`HTML!`](scalars.md#html) | The body rendered to HTML. |
| `bodyText` | [`String!`](scalars.md#string) | The body rendered to text. |
| `createdAt` | [`DateTime!`](scalars.md#datetime) | Identifies the date and time when the object was created. |
| `createdViaEmail` | [`Boolean!`](scalars.md#boolean) | Check if this comment was created via an email reply. |
| `editor` | [`Actor`](interfaces.md#actor) | The actor who edited the comment. |
| `id` | [`ID!`](scalars.md#id) |  |
| `includesCreatedEdit` | [`Boolean!`](scalars.md#boolean) | Check if this comment was edited and includes an edit with the creation data |
| `lastEditedAt` | [`DateTime`](scalars.md#datetime) | The moment the editor made the last edit |
| `publishedAt` | [`DateTime`](scalars.md#datetime) | Identifies when the comment was published at. |
| `updatedAt` | [`DateTime!`](scalars.md#datetime) | Identifies the date and time when the object was last updated. |
| `userContentEdits` | [`UserContentEditConnection`](objects.md#usercontenteditconnection) | A list of edits to this content. |
| `viewerDidAuthor` | [`Boolean!`](scalars.md#boolean) | Did the viewer author this comment. |

### Implemented by

* `CommitComment`
* `GistComment`
* `IssueComment`
* `Issue`
* `PullRequestReviewComment`
* `PullRequestReview`
* `PullRequest`
* `TeamDiscussionComment`
* `TeamDiscussion`

## <a name="contribution"></a>Contribution

> Represents a contribution a user made on GitHub, such as opening an issue.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `isRestricted` | [`Boolean!`](scalars.md#boolean) | Whether this contribution is associated with a record you do not have access to. Forexample, your own 'first issue' contribution may have been made on a repository you can nolonger access. |
| `occurredAt` | [`DateTime!`](scalars.md#datetime) | When this contribution was made. |
| `resourcePath` | [`URI!`](scalars.md#uri) | The HTTP path for this contribution. |
| `url` | [`URI!`](scalars.md#uri) | The HTTP URL for this contribution. |
| `user` | [`User!`](objects.md#user) | The user who made this contribution. |

### Implemented by

* `CreatedCommitContribution`
* `CreatedIssueContribution`
* `CreatedPullRequestContribution`
* `CreatedPullRequestReviewContribution`
* `CreatedRepositoryContribution`
* `JoinedGitHubContribution`
* `RestrictedContribution`

## <a name="deletable"></a>Deletable

> Entities that can be deleted.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `viewerCanDelete` | [`Boolean!`](scalars.md#boolean) | Check if the current viewer can delete this object. |

### Implemented by

* `CommitComment`
* `GistComment`
* `IssueComment`
* `PullRequestReviewComment`
* `PullRequestReview`
* `TeamDiscussionComment`
* `TeamDiscussion`

## <a name="enterpriseauditentrydata"></a>EnterpriseAuditEntryData

> Metadata for an audit entry containing enterprise account information.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `enterpriseResourcePath` | [`URI`](scalars.md#uri) | The HTTP path for this enterprise. |
| `enterpriseSlug` | [`String`](scalars.md#string) | The slug of the enterprise. |
| `enterpriseUrl` | [`URI`](scalars.md#uri) | The HTTP URL for this enterprise. |

### Implemented by

* `MembersCanDeleteReposClearAuditEntry`
* `MembersCanDeleteReposDisableAuditEntry`
* `MembersCanDeleteReposEnableAuditEntry`
* `OrgInviteToBusinessAuditEntry`
* `PrivateRepositoryForkingDisableAuditEntry`
* `PrivateRepositoryForkingEnableAuditEntry`
* `RepositoryVisibilityChangeDisableAuditEntry`
* `RepositoryVisibilityChangeEnableAuditEntry`

## <a name="gitobject"></a>GitObject

> Represents a Git object.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `abbreviatedOid` | [`String!`](scalars.md#string) | An abbreviated version of the Git object ID |
| `commitResourcePath` | [`URI!`](scalars.md#uri) | The HTTP path for this Git object |
| `commitUrl` | [`URI!`](scalars.md#uri) | The HTTP URL for this Git object |
| `id` | [`ID!`](scalars.md#id) |  |
| `oid` | [`GitObjectID!`](scalars.md#gitobjectid) | The Git object ID |
| `repository` | [`Repository!`](objects.md#repository) | The Repository the Git object belongs to |

### Implemented by

* `Blob`
* `Commit`
* `Tag`
* `Tree`

## <a name="gitsignature"></a>GitSignature

> Information about a signature (GPG or S/MIME) on a Commit or Tag.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `email` | [`String!`](scalars.md#string) | Email used to sign this object. |
| `isValid` | [`Boolean!`](scalars.md#boolean) | True if the signature is valid and verified by GitHub. |
| `payload` | [`String!`](scalars.md#string) | Payload for GPG signing object. Raw ODB object without the signature header. |
| `signature` | [`String!`](scalars.md#string) | ASCII-armored signature header from object. |
| `signer` | [`User`](objects.md#user) | GitHub user corresponding to the email signing this commit. |
| `state` | [`GitSignatureState!`](enums.md#gitsignaturestate) | The state of this signature. `VALID` if signature is valid and verified by GitHub, otherwise represents reason why signature is considered invalid. |
| `wasSignedByGitHub` | [`Boolean!`](scalars.md#boolean) | True if the signature was made with GitHub's signing key. |

### Implemented by

* `GpgSignature`
* `SmimeSignature`
* `UnknownSignature`

## <a name="hovercardcontext"></a>HovercardContext

> An individual line of a hovercard

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `message` | [`String!`](scalars.md#string) | A string describing this context |
| `octicon` | [`String!`](scalars.md#string) | An octicon to accompany this context |

### Implemented by

* `GenericHovercardContext`
* `OrganizationTeamsHovercardContext`
* `OrganizationsHovercardContext`
* `ReviewStatusHovercardContext`
* `ViewerHovercardContext`

## <a name="labelable"></a>Labelable

> An object that can have labels assigned to it.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `labels` | [`LabelConnection`](objects.md#labelconnection) | A list of labels associated with the object. |

### Implemented by

* `Issue`
* `PullRequest`

## <a name="lockable"></a>Lockable

> An object that can be locked.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `activeLockReason` | [`LockReason`](enums.md#lockreason) | Reason that the conversation was locked. |
| `locked` | [`Boolean!`](scalars.md#boolean) | `true` if the object is locked |

### Implemented by

* `Issue`
* `PullRequest`

## <a name="memberstatusable"></a>MemberStatusable

> Entities that have members who can set status messages.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `memberStatuses` | [`UserStatusConnection!`](objects.md#userstatusconnection) | Get the status messages members of this entity have set that are either public or visible only to the organization. |

### Implemented by

* `Organization`
* `Team`

## <a name="minimizable"></a>Minimizable

> Entities that can be minimized.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `isMinimized` | [`Boolean!`](scalars.md#boolean) | Returns whether or not a comment has been minimized. |
| `minimizedReason` | [`String`](scalars.md#string) | Returns why the comment was minimized. |
| `viewerCanMinimize` | [`Boolean!`](scalars.md#boolean) | Check if the current viewer can minimize this object. |

### Implemented by

* `CommitComment`
* `GistComment`
* `IssueComment`
* `PullRequestReviewComment`

## <a name="node"></a>Node

> An object with an ID.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `id` | [`ID!`](scalars.md#id) | ID of the object. |

### Implemented by

* `AddedToProjectEvent`
* `App`
* `AssignedEvent`
* `AutomaticBaseChangeFailedEvent`
* `AutomaticBaseChangeSucceededEvent`
* `BaseRefChangedEvent`
* `BaseRefForcePushedEvent`
* `Blob`
* `Bot`
* `BranchProtectionRule`
* `ClosedEvent`
* `CodeOfConduct`
* `CommentDeletedEvent`
* `CommitCommentThread`
* `CommitComment`
* `Commit`
* `ConnectedEvent`
* `ConvertToDraftEvent`
* `ConvertedNoteToIssueEvent`
* `CrossReferencedEvent`
* `DemilestonedEvent`
* `DeployKey`
* `DeployedEvent`
* `DeploymentEnvironmentChangedEvent`
* `DeploymentStatus`
* `Deployment`
* `DisconnectedEvent`
* `EnterpriseAdministratorInvitation`
* `EnterpriseIdentityProvider`
* `EnterpriseRepositoryInfo`
* `EnterpriseServerInstallation`
* `EnterpriseServerUserAccountEmail`
* `EnterpriseServerUserAccount`
* `EnterpriseServerUserAccountsUpload`
* `EnterpriseUserAccount`
* `Enterprise`
* `ExternalIdentity`
* `GistComment`
* `Gist`
* `HeadRefDeletedEvent`
* `HeadRefForcePushedEvent`
* `HeadRefRestoredEvent`
* `IpAllowListEntry`
* `IssueComment`
* `Issue`
* `Label`
* `LabeledEvent`
* `Language`
* `License`
* `LockedEvent`
* `Mannequin`
* `MarkedAsDuplicateEvent`
* `MarketplaceCategory`
* `MarketplaceListing`
* `MembersCanDeleteReposClearAuditEntry`
* `MembersCanDeleteReposDisableAuditEntry`
* `MembersCanDeleteReposEnableAuditEntry`
* `MentionedEvent`
* `MergedEvent`
* `Milestone`
* `MilestonedEvent`
* `MovedColumnsInProjectEvent`
* `OauthApplicationCreateAuditEntry`
* `OrgAddBillingManagerAuditEntry`
* `OrgAddMemberAuditEntry`
* `OrgBlockUserAuditEntry`
* `OrgConfigDisableCollaboratorsOnlyAuditEntry`
* `OrgConfigEnableCollaboratorsOnlyAuditEntry`
* `OrgCreateAuditEntry`
* `OrgDisableOauthAppRestrictionsAuditEntry`
* `OrgDisableSamlAuditEntry`
* `OrgDisableTwoFactorRequirementAuditEntry`
* `OrgEnableOauthAppRestrictionsAuditEntry`
* `OrgEnableSamlAuditEntry`
* `OrgEnableTwoFactorRequirementAuditEntry`
* `OrgInviteMemberAuditEntry`
* `OrgInviteToBusinessAuditEntry`
* `OrgOauthAppAccessApprovedAuditEntry`
* `OrgOauthAppAccessDeniedAuditEntry`
* `OrgOauthAppAccessRequestedAuditEntry`
* `OrgRemoveBillingManagerAuditEntry`
* `OrgRemoveMemberAuditEntry`
* `OrgRemoveOutsideCollaboratorAuditEntry`
* `OrgRestoreMemberAuditEntry`
* `OrgUnblockUserAuditEntry`
* `OrgUpdateDefaultRepositoryPermissionAuditEntry`
* `OrgUpdateMemberAuditEntry`
* `OrgUpdateMemberRepositoryCreationPermissionAuditEntry`
* `OrgUpdateMemberRepositoryInvitationPermissionAuditEntry`
* `OrganizationIdentityProvider`
* `OrganizationInvitation`
* `Organization`
* `PackageFile`
* `PackageTag`
* `PackageVersion`
* `Package`
* `PinnedEvent`
* `PrivateRepositoryForkingDisableAuditEntry`
* `PrivateRepositoryForkingEnableAuditEntry`
* `ProjectCard`
* `ProjectColumn`
* `Project`
* `PublicKey`
* `PullRequestCommitCommentThread`
* `PullRequestCommit`
* `PullRequestReviewComment`
* `PullRequestReviewThread`
* `PullRequestReview`
* `PullRequest`
* `PushAllowance`
* `Reaction`
* `ReadyForReviewEvent`
* `Ref`
* `ReferencedEvent`
* `ReleaseAsset`
* `Release`
* `RemovedFromProjectEvent`
* `RenamedTitleEvent`
* `ReopenedEvent`
* `RepoAccessAuditEntry`
* `RepoAddMemberAuditEntry`
* `RepoAddTopicAuditEntry`
* `RepoArchivedAuditEntry`
* `RepoChangeMergeSettingAuditEntry`
* `RepoConfigDisableAnonymousGitAccessAuditEntry`
* `RepoConfigDisableCollaboratorsOnlyAuditEntry`
* `RepoConfigDisableContributorsOnlyAuditEntry`
* `RepoConfigDisableSockpuppetDisallowedAuditEntry`
* `RepoConfigEnableAnonymousGitAccessAuditEntry`
* `RepoConfigEnableCollaboratorsOnlyAuditEntry`
* `RepoConfigEnableContributorsOnlyAuditEntry`
* `RepoConfigEnableSockpuppetDisallowedAuditEntry`
* `RepoConfigLockAnonymousGitAccessAuditEntry`
* `RepoConfigUnlockAnonymousGitAccessAuditEntry`
* `RepoCreateAuditEntry`
* `RepoDestroyAuditEntry`
* `RepoRemoveMemberAuditEntry`
* `RepoRemoveTopicAuditEntry`
* `RepositoryInvitation`
* `RepositoryTopic`
* `RepositoryVisibilityChangeDisableAuditEntry`
* `RepositoryVisibilityChangeEnableAuditEntry`
* `RepositoryVulnerabilityAlert`
* `Repository`
* `ReviewDismissalAllowance`
* `ReviewDismissedEvent`
* `ReviewRequestRemovedEvent`
* `ReviewRequest`
* `ReviewRequestedEvent`
* `SavedReply`
* `SecurityAdvisory`
* `SponsorsListing`
* `SponsorsTier`
* `Sponsorship`
* `StatusCheckRollup`
* `StatusContext`
* `Status`
* `SubscribedEvent`
* `Tag`
* `TeamAddMemberAuditEntry`
* `TeamAddRepositoryAuditEntry`
* `TeamChangeParentTeamAuditEntry`
* `TeamDiscussionComment`
* `TeamDiscussion`
* `TeamRemoveMemberAuditEntry`
* `TeamRemoveRepositoryAuditEntry`
* `Team`
* `Topic`
* `TransferredEvent`
* `Tree`
* `UnassignedEvent`
* `UnlabeledEvent`
* `UnlockedEvent`
* `UnmarkedAsDuplicateEvent`
* `UnpinnedEvent`
* `UnsubscribedEvent`
* `UserBlockedEvent`
* `UserContentEdit`
* `UserStatus`
* `User`

## <a name="oauthapplicationauditentrydata"></a>OauthApplicationAuditEntryData

> Metadata for an audit entry with action oauth_application.*

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `oauthApplicationName` | [`String`](scalars.md#string) | The name of the OAuth Application. |
| `oauthApplicationResourcePath` | [`URI`](scalars.md#uri) | The HTTP path for the OAuth Application |
| `oauthApplicationUrl` | [`URI`](scalars.md#uri) | The HTTP URL for the OAuth Application |

### Implemented by

* `OauthApplicationCreateAuditEntry`
* `OrgOauthAppAccessApprovedAuditEntry`
* `OrgOauthAppAccessDeniedAuditEntry`
* `OrgOauthAppAccessRequestedAuditEntry`

## <a name="organizationauditentrydata"></a>OrganizationAuditEntryData

> Metadata for an audit entry with action org.*

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `organization` | [`Organization`](objects.md#organization) | The Organization associated with the Audit Entry. |
| `organizationName` | [`String`](scalars.md#string) | The name of the Organization. |
| `organizationResourcePath` | [`URI`](scalars.md#uri) | The HTTP path for the organization |
| `organizationUrl` | [`URI`](scalars.md#uri) | The HTTP URL for the organization |

### Implemented by

* `MembersCanDeleteReposClearAuditEntry`
* `MembersCanDeleteReposDisableAuditEntry`
* `MembersCanDeleteReposEnableAuditEntry`
* `OauthApplicationCreateAuditEntry`
* `OrgAddBillingManagerAuditEntry`
* `OrgAddMemberAuditEntry`
* `OrgBlockUserAuditEntry`
* `OrgConfigDisableCollaboratorsOnlyAuditEntry`
* `OrgConfigEnableCollaboratorsOnlyAuditEntry`
* `OrgCreateAuditEntry`
* `OrgDisableOauthAppRestrictionsAuditEntry`
* `OrgDisableSamlAuditEntry`
* `OrgDisableTwoFactorRequirementAuditEntry`
* `OrgEnableOauthAppRestrictionsAuditEntry`
* `OrgEnableSamlAuditEntry`
* `OrgEnableTwoFactorRequirementAuditEntry`
* `OrgInviteMemberAuditEntry`
* `OrgInviteToBusinessAuditEntry`
* `OrgOauthAppAccessApprovedAuditEntry`
* `OrgOauthAppAccessDeniedAuditEntry`
* `OrgOauthAppAccessRequestedAuditEntry`
* `OrgRemoveBillingManagerAuditEntry`
* `OrgRemoveMemberAuditEntry`
* `OrgRemoveOutsideCollaboratorAuditEntry`
* `OrgRestoreMemberAuditEntry`
* `OrgRestoreMemberMembershipOrganizationAuditEntryData`
* `OrgUnblockUserAuditEntry`
* `OrgUpdateDefaultRepositoryPermissionAuditEntry`
* `OrgUpdateMemberAuditEntry`
* `OrgUpdateMemberRepositoryCreationPermissionAuditEntry`
* `OrgUpdateMemberRepositoryInvitationPermissionAuditEntry`
* `PrivateRepositoryForkingDisableAuditEntry`
* `PrivateRepositoryForkingEnableAuditEntry`
* `RepoAccessAuditEntry`
* `RepoAddMemberAuditEntry`
* `RepoAddTopicAuditEntry`
* `RepoArchivedAuditEntry`
* `RepoChangeMergeSettingAuditEntry`
* `RepoConfigDisableAnonymousGitAccessAuditEntry`
* `RepoConfigDisableCollaboratorsOnlyAuditEntry`
* `RepoConfigDisableContributorsOnlyAuditEntry`
* `RepoConfigDisableSockpuppetDisallowedAuditEntry`
* `RepoConfigEnableAnonymousGitAccessAuditEntry`
* `RepoConfigEnableCollaboratorsOnlyAuditEntry`
* `RepoConfigEnableContributorsOnlyAuditEntry`
* `RepoConfigEnableSockpuppetDisallowedAuditEntry`
* `RepoConfigLockAnonymousGitAccessAuditEntry`
* `RepoConfigUnlockAnonymousGitAccessAuditEntry`
* `RepoCreateAuditEntry`
* `RepoDestroyAuditEntry`
* `RepoRemoveMemberAuditEntry`
* `RepoRemoveTopicAuditEntry`
* `RepositoryVisibilityChangeDisableAuditEntry`
* `RepositoryVisibilityChangeEnableAuditEntry`
* `TeamAddMemberAuditEntry`
* `TeamAddRepositoryAuditEntry`
* `TeamChangeParentTeamAuditEntry`
* `TeamRemoveMemberAuditEntry`
* `TeamRemoveRepositoryAuditEntry`

## <a name="packageowner"></a>PackageOwner

> Represents an owner of a package.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `id` | [`ID!`](scalars.md#id) |  |
| `packages` | [`PackageConnection!`](objects.md#packageconnection) | A list of packages under the owner. |

### Implemented by

* `Organization`
* `Repository`
* `User`

## <a name="profileowner"></a>ProfileOwner

> Represents any entity on GitHub that has a profile page.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `anyPinnableItems` | [`Boolean!`](scalars.md#boolean) | Determine if this repository owner has any items that can be pinned to their profile. |
| `email` | [`String`](scalars.md#string) | The public profile email. |
| `id` | [`ID!`](scalars.md#id) |  |
| `itemShowcase` | [`ProfileItemShowcase!`](objects.md#profileitemshowcase) | Showcases a selection of repositories and gists that the profile owner has either curated or that have been selected automatically based on popularity. |
| `location` | [`String`](scalars.md#string) | The public profile location. |
| `login` | [`String!`](scalars.md#string) | The username used to login. |
| `name` | [`String`](scalars.md#string) | The public profile name. |
| `pinnableItems` | [`PinnableItemConnection!`](objects.md#pinnableitemconnection) | A list of repositories and gists this profile owner can pin to their profile. |
| `pinnedItems` | [`PinnableItemConnection!`](objects.md#pinnableitemconnection) | A list of repositories and gists this profile owner has pinned to their profile |
| `pinnedItemsRemaining` | [`Int!`](scalars.md#int) | Returns how many more items this profile owner can pin to their profile. |
| `viewerCanChangePinnedItems` | [`Boolean!`](scalars.md#boolean) | Can the viewer pin repositories and gists to the profile? |
| `websiteUrl` | [`URI`](scalars.md#uri) | The public profile website URL. |

### Implemented by

* `Organization`
* `User`

## <a name="projectowner"></a>ProjectOwner

> Represents an owner of a Project.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `id` | [`ID!`](scalars.md#id) |  |
| `project` | [`Project`](objects.md#project) | Find project by number. |
| `projects` | [`ProjectConnection!`](objects.md#projectconnection) | A list of projects under the owner. |
| `projectsResourcePath` | [`URI!`](scalars.md#uri) | The HTTP path listing owners projects |
| `projectsUrl` | [`URI!`](scalars.md#uri) | The HTTP URL listing owners projects |
| `viewerCanCreateProjects` | [`Boolean!`](scalars.md#boolean) | Can the current viewer create new projects on this owner. |

### Implemented by

* `Organization`
* `Repository`
* `User`

## <a name="reactable"></a>Reactable

> Represents a subject that can be reacted on.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `databaseId` | [`Int`](scalars.md#int) | Identifies the primary key from the database. |
| `id` | [`ID!`](scalars.md#id) |  |
| `reactionGroups` | [`[ReactionGroup!]`](objects.md#reactiongroup) | A list of reactions grouped by content left on the subject. |
| `reactions` | [`ReactionConnection!`](objects.md#reactionconnection) | A list of Reactions left on the Issue. |
| `viewerCanReact` | [`Boolean!`](scalars.md#boolean) | Can user react to this subject |

### Implemented by

* `CommitComment`
* `IssueComment`
* `Issue`
* `PullRequestReviewComment`
* `PullRequestReview`
* `PullRequest`
* `TeamDiscussionComment`
* `TeamDiscussion`

## <a name="repositoryauditentrydata"></a>RepositoryAuditEntryData

> Metadata for an audit entry with action repo.*

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `repository` | [`Repository`](objects.md#repository) | The repository associated with the action |
| `repositoryName` | [`String`](scalars.md#string) | The name of the repository |
| `repositoryResourcePath` | [`URI`](scalars.md#uri) | The HTTP path for the repository |
| `repositoryUrl` | [`URI`](scalars.md#uri) | The HTTP URL for the repository |

### Implemented by

* `OrgRestoreMemberMembershipRepositoryAuditEntryData`
* `PrivateRepositoryForkingDisableAuditEntry`
* `PrivateRepositoryForkingEnableAuditEntry`
* `RepoAccessAuditEntry`
* `RepoAddMemberAuditEntry`
* `RepoAddTopicAuditEntry`
* `RepoArchivedAuditEntry`
* `RepoChangeMergeSettingAuditEntry`
* `RepoConfigDisableAnonymousGitAccessAuditEntry`
* `RepoConfigDisableCollaboratorsOnlyAuditEntry`
* `RepoConfigDisableContributorsOnlyAuditEntry`
* `RepoConfigDisableSockpuppetDisallowedAuditEntry`
* `RepoConfigEnableAnonymousGitAccessAuditEntry`
* `RepoConfigEnableCollaboratorsOnlyAuditEntry`
* `RepoConfigEnableContributorsOnlyAuditEntry`
* `RepoConfigEnableSockpuppetDisallowedAuditEntry`
* `RepoConfigLockAnonymousGitAccessAuditEntry`
* `RepoConfigUnlockAnonymousGitAccessAuditEntry`
* `RepoCreateAuditEntry`
* `RepoDestroyAuditEntry`
* `RepoRemoveMemberAuditEntry`
* `RepoRemoveTopicAuditEntry`
* `TeamAddRepositoryAuditEntry`
* `TeamRemoveRepositoryAuditEntry`

## <a name="repositoryinfo"></a>RepositoryInfo

> A subset of repository info.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `createdAt` | [`DateTime!`](scalars.md#datetime) | Identifies the date and time when the object was created. |
| `description` | [`String`](scalars.md#string) | The description of the repository. |
| `descriptionHTML` | [`HTML!`](scalars.md#html) | The description of the repository rendered to HTML. |
| `forkCount` | [`Int!`](scalars.md#int) | Returns how many forks there are of this repository in the whole network. |
| `hasIssuesEnabled` | [`Boolean!`](scalars.md#boolean) | Indicates if the repository has issues feature enabled. |
| `hasProjectsEnabled` | [`Boolean!`](scalars.md#boolean) | Indicates if the repository has the Projects feature enabled. |
| `hasWikiEnabled` | [`Boolean!`](scalars.md#boolean) | Indicates if the repository has wiki feature enabled. |
| `homepageUrl` | [`URI`](scalars.md#uri) | The repository's URL. |
| `isArchived` | [`Boolean!`](scalars.md#boolean) | Indicates if the repository is unmaintained. |
| `isFork` | [`Boolean!`](scalars.md#boolean) | Identifies if the repository is a fork. |
| `isLocked` | [`Boolean!`](scalars.md#boolean) | Indicates if the repository has been locked or not. |
| `isMirror` | [`Boolean!`](scalars.md#boolean) | Identifies if the repository is a mirror. |
| `isPrivate` | [`Boolean!`](scalars.md#boolean) | Identifies if the repository is private. |
| `isTemplate` | [`Boolean!`](scalars.md#boolean) | Identifies if the repository is a template that can be used to generate new repositories. |
| `licenseInfo` | [`License`](objects.md#license) | The license associated with the repository |
| `lockReason` | [`RepositoryLockReason`](enums.md#repositorylockreason) | The reason the repository has been locked. |
| `mirrorUrl` | [`URI`](scalars.md#uri) | The repository's original mirror URL. |
| `name` | [`String!`](scalars.md#string) | The name of the repository. |
| `nameWithOwner` | [`String!`](scalars.md#string) | The repository's name with owner. |
| `openGraphImageUrl` | [`URI!`](scalars.md#uri) | The image used to represent this repository in Open Graph data. |
| `owner` | [`RepositoryOwner!`](interfaces.md#repositoryowner) | The User owner of the repository. |
| `pushedAt` | [`DateTime`](scalars.md#datetime) | Identifies when the repository was last pushed to. |
| `resourcePath` | [`URI!`](scalars.md#uri) | The HTTP path for this repository |
| `shortDescriptionHTML` | [`HTML!`](scalars.md#html) | A description of the repository, rendered to HTML without any links in it. |
| `updatedAt` | [`DateTime!`](scalars.md#datetime) | Identifies the date and time when the object was last updated. |
| `url` | [`URI!`](scalars.md#uri) | The HTTP URL for this repository |
| `usesCustomOpenGraphImage` | [`Boolean!`](scalars.md#boolean) | Whether this repository has a custom image to use with Open Graph as opposed to being represented by the owner's avatar. |

### Implemented by

* `Repository`

## <a name="repositorynode"></a>RepositoryNode

> Represents a object that belongs to a repository.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `repository` | [`Repository!`](objects.md#repository) | The repository associated with this node. |

### Implemented by

* `CommitCommentThread`
* `CommitComment`
* `IssueComment`
* `Issue`
* `PullRequestCommitCommentThread`
* `PullRequestReviewComment`
* `PullRequestReview`
* `PullRequest`
* `RepositoryVulnerabilityAlert`

## <a name="repositoryowner"></a>RepositoryOwner

> Represents an owner of a Repository.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `avatarUrl` | [`URI!`](scalars.md#uri) | A URL pointing to the owner's public avatar. |
| `id` | [`ID!`](scalars.md#id) |  |
| `login` | [`String!`](scalars.md#string) | The username used to login. |
| `repositories` | [`RepositoryConnection!`](objects.md#repositoryconnection) | A list of repositories that the user owns. |
| `repository` | [`Repository`](objects.md#repository) | Find Repository. |
| `resourcePath` | [`URI!`](scalars.md#uri) | The HTTP URL for the owner. |
| `url` | [`URI!`](scalars.md#uri) | The HTTP URL for the owner. |

### Implemented by

* `Organization`
* `User`

## <a name="sponsorable"></a>Sponsorable

> Entities that can be sponsored through GitHub Sponsors

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `sponsorsListing` | [`SponsorsListing`](objects.md#sponsorslisting) | The GitHub Sponsors listing for this user. |
| `sponsorshipsAsMaintainer` | [`SponsorshipConnection!`](objects.md#sponsorshipconnection) | This object's sponsorships as the maintainer. |
| `sponsorshipsAsSponsor` | [`SponsorshipConnection!`](objects.md#sponsorshipconnection) | This object's sponsorships as the sponsor. |

### Implemented by

* `Organization`
* `User`

## <a name="starrable"></a>Starrable

> Things that can be starred.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `id` | [`ID!`](scalars.md#id) |  |
| `stargazers` | [`StargazerConnection!`](objects.md#stargazerconnection) | A list of users who have starred this starrable. |
| `viewerHasStarred` | [`Boolean!`](scalars.md#boolean) | Returns a boolean indicating whether the viewing user has starred this starrable. |

### Implemented by

* `Gist`
* `Repository`
* `Topic`

## <a name="subscribable"></a>Subscribable

> Entities that can be subscribed to for web and email notifications.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `id` | [`ID!`](scalars.md#id) |  |
| `viewerCanSubscribe` | [`Boolean!`](scalars.md#boolean) | Check if the viewer is able to change their subscription status for the repository. |
| `viewerSubscription` | [`SubscriptionState`](enums.md#subscriptionstate) | Identifies if the viewer is watching, not watching, or ignoring the subscribable entity. |

### Implemented by

* `Commit`
* `Issue`
* `PullRequest`
* `Repository`
* `TeamDiscussion`
* `Team`

## <a name="teamauditentrydata"></a>TeamAuditEntryData

> Metadata for an audit entry with action team.*

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `team` | [`Team`](objects.md#team) | The team associated with the action |
| `teamName` | [`String`](scalars.md#string) | The name of the team |
| `teamResourcePath` | [`URI`](scalars.md#uri) | The HTTP path for this team |
| `teamUrl` | [`URI`](scalars.md#uri) | The HTTP URL for this team |

### Implemented by

* `OrgRestoreMemberMembershipTeamAuditEntryData`
* `TeamAddMemberAuditEntry`
* `TeamAddRepositoryAuditEntry`
* `TeamChangeParentTeamAuditEntry`
* `TeamRemoveMemberAuditEntry`
* `TeamRemoveRepositoryAuditEntry`

## <a name="topicauditentrydata"></a>TopicAuditEntryData

> Metadata for an audit entry with a topic.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `topic` | [`Topic`](objects.md#topic) | The name of the topic added to the repository |
| `topicName` | [`String`](scalars.md#string) | The name of the topic added to the repository |

### Implemented by

* `RepoAddTopicAuditEntry`
* `RepoRemoveTopicAuditEntry`

## <a name="uniformresourcelocatable"></a>UniformResourceLocatable

> Represents a type that can be retrieved by a URL.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `resourcePath` | [`URI!`](scalars.md#uri) | The HTML path to this resource. |
| `url` | [`URI!`](scalars.md#uri) | The URL to this resource. |

### Implemented by

* `Bot`
* `ClosedEvent`
* `Commit`
* `ConvertToDraftEvent`
* `CrossReferencedEvent`
* `Gist`
* `Issue`
* `Mannequin`
* `MergedEvent`
* `Milestone`
* `Organization`
* `PullRequestCommit`
* `PullRequest`
* `ReadyForReviewEvent`
* `Release`
* `RepositoryTopic`
* `Repository`
* `ReviewDismissedEvent`
* `TeamDiscussionComment`
* `TeamDiscussion`
* `User`

## <a name="updatable"></a>Updatable

> Entities that can be updated.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `viewerCanUpdate` | [`Boolean!`](scalars.md#boolean) | Check if the current viewer can update this object. |

### Implemented by

* `CommitComment`
* `GistComment`
* `IssueComment`
* `Issue`
* `Project`
* `PullRequestReviewComment`
* `PullRequestReview`
* `PullRequest`
* `TeamDiscussionComment`
* `TeamDiscussion`

## <a name="updatablecomment"></a>UpdatableComment

> Comments that can be updated.

### Fields

| Name | Type | Description |
| --- | --- | --- |
| `viewerCannotUpdateReasons` | [`[CommentCannotUpdateReason!]!`](enums.md#commentcannotupdatereason) | Reasons why the current viewer can not update this comment. |

### Implemented by

* `CommitComment`
* `GistComment`
* `IssueComment`
* `Issue`
* `PullRequestReviewComment`
* `PullRequestReview`
* `PullRequest`
* `TeamDiscussionComment`
* `TeamDiscussion`

