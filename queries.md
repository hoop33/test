# Query

> The query root of GitHub's GraphQL interface.

## codeOfConduct

> Look up a code of conduct by its key

**Type:** [`CodeOfConduct`](objects.md#codeofconduct)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `key` | [`String!`](scalars.md#string) | The code of conduct's key |  |

## codesOfConduct

> Look up a code of conduct by its key

**Type:** [`[CodeOfConduct]`](objects.md#codeofconduct)

## enterprise

> Look up an enterprise by URL slug.

**Type:** [`Enterprise`](objects.md#enterprise)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `invitationToken` | [`String`](scalars.md#string) | The enterprise invitation token. |  |
| `slug` | [`String!`](scalars.md#string) | The enterprise URL slug. |  |

## enterpriseAdministratorInvitation

> Look up a pending enterprise administrator invitation by invitee, enterprise and role.

**Type:** [`EnterpriseAdministratorInvitation`](objects.md#enterpriseadministratorinvitation)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `enterpriseSlug` | [`String!`](scalars.md#string) | The slug of the enterprise the user was invited to join. |  |
| `role` | [`EnterpriseAdministratorRole!`](enums.md#enterpriseadministratorrole) | The role for the business member invitation. |  |
| `userLogin` | [`String!`](scalars.md#string) | The login of the user invited to join the business. |  |

## enterpriseAdministratorInvitationByToken

> Look up a pending enterprise administrator invitation by invitation token.

**Type:** [`EnterpriseAdministratorInvitation`](objects.md#enterpriseadministratorinvitation)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `invitationToken` | [`String!`](scalars.md#string) | The invitation token sent with the invitation email. |  |

## license

> Look up an open source license by its key

**Type:** [`License`](objects.md#license)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `key` | [`String!`](scalars.md#string) | The license's downcased SPDX ID |  |

## licenses

> Return a list of known open source licenses

**Type:** [`[License]!`](objects.md#license)

## marketplaceCategories

> Get alphabetically sorted list of Marketplace categories

**Type:** [`[MarketplaceCategory!]!`](objects.md#marketplacecategory)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `excludeEmpty` | [`Boolean`](scalars.md#boolean) | Exclude categories with no listings. |  |
| `excludeSubcategories` | [`Boolean`](scalars.md#boolean) | Returns top level categories only, excluding any subcategories. |  |
| `includeCategories` | [`[String!]`](scalars.md#string) | Return only the specified categories. |  |

## marketplaceCategory

> Look up a Marketplace category by its slug.

**Type:** [`MarketplaceCategory`](objects.md#marketplacecategory)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `slug` | [`String!`](scalars.md#string) | The URL slug of the category. |  |
| `useTopicAliases` | [`Boolean`](scalars.md#boolean) | Also check topic aliases for the category slug |  |

## marketplaceListing

> Look up a single Marketplace listing

**Type:** [`MarketplaceListing`](objects.md#marketplacelisting)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `slug` | [`String!`](scalars.md#string) | Select the listing that matches this slug. It's the short name of the listing used in its URL. |  |

## marketplaceListings

> Look up Marketplace listings

**Type:** [`MarketplaceListingConnection!`](objects.md#marketplacelistingconnection)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `adminId` | [`ID`](scalars.md#id) | Select listings that can be administered by the specified user. |  |
| `after` | [`String`](scalars.md#string) | Returns the elements in the list that come after the specified cursor. |  |
| `allStates` | [`Boolean`](scalars.md#boolean) | Select listings visible to the viewer even if they are not approved. If omitted orfalse, only approved listings will be returned. |  |
| `before` | [`String`](scalars.md#string) | Returns the elements in the list that come before the specified cursor. |  |
| `categorySlug` | [`String`](scalars.md#string) | Select only listings with the given category. |  |
| `first` | [`Int`](scalars.md#int) | Returns the first _n_ elements from the list. |  |
| `last` | [`Int`](scalars.md#int) | Returns the last _n_ elements from the list. |  |
| `organizationId` | [`ID`](scalars.md#id) | Select listings for products owned by the specified organization. |  |
| `primaryCategoryOnly` | [`Boolean`](scalars.md#boolean) | Select only listings where the primary category matches the given category slug. | `false` |
| `slugs` | [`[String]`](scalars.md#string) | Select the listings with these slugs, if they are visible to the viewer. |  |
| `useTopicAliases` | [`Boolean`](scalars.md#boolean) | Also check topic aliases for the category slug |  |
| `viewerCanAdmin` | [`Boolean`](scalars.md#boolean) | Select listings to which user has admin access. If omitted, listings visible to theviewer are returned. |  |
| `withFreeTrialsOnly` | [`Boolean`](scalars.md#boolean) | Select only listings that offer a free trial. | `false` |

## meta

> Return information about the GitHub instance

**Type:** [`GitHubMetadata!`](objects.md#githubmetadata)

## node

> Fetches an object given its ID.

**Type:** [`Node`](interfaces.md#node)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `id` | [`ID!`](scalars.md#id) | ID of the object. |  |

## nodes

> Lookup nodes by a list of IDs.

**Type:** [`[Node]!`](interfaces.md#node)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `ids` | [`[ID!]!`](scalars.md#id) | The list of node IDs. |  |

## organization

> Lookup a organization by login.

**Type:** [`Organization`](objects.md#organization)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `login` | [`String!`](scalars.md#string) | The organization's login. |  |

## rateLimit

> The client's rate limit information.

**Type:** [`RateLimit`](objects.md#ratelimit)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `dryRun` | [`Boolean`](scalars.md#boolean) | If true, calculate the cost for the query without evaluating it | `false` |

## relay

> Hack to workaround https://github.com/facebook/relay/issues/112 re-exposing the root query object

**Type:** [`Query!`](objects.md#query)

## repository

> Lookup a given repository by the owner and repository name.

**Type:** [`Repository`](objects.md#repository)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `name` | [`String!`](scalars.md#string) | The name of the repository |  |
| `owner` | [`String!`](scalars.md#string) | The login field of a user or organization |  |

## repositoryOwner

> Lookup a repository owner (ie. either a User or an Organization) by login.

**Type:** [`RepositoryOwner`](interfaces.md#repositoryowner)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `login` | [`String!`](scalars.md#string) | The username to lookup the owner by. |  |

## resource

> Lookup resource by a URL.

**Type:** [`UniformResourceLocatable`](interfaces.md#uniformresourcelocatable)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `url` | [`URI!`](scalars.md#uri) | The URL. |  |

## search

> Perform a search across resources.

**Type:** [`SearchResultItemConnection!`](objects.md#searchresultitemconnection)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `after` | [`String`](scalars.md#string) | Returns the elements in the list that come after the specified cursor. |  |
| `before` | [`String`](scalars.md#string) | Returns the elements in the list that come before the specified cursor. |  |
| `first` | [`Int`](scalars.md#int) | Returns the first _n_ elements from the list. |  |
| `last` | [`Int`](scalars.md#int) | Returns the last _n_ elements from the list. |  |
| `query` | [`String!`](scalars.md#string) | The search string to look for. |  |
| `type` | [`SearchType!`](enums.md#searchtype) | The types of search items to search within. |  |

## securityAdvisories

> GitHub Security Advisories

**Type:** [`SecurityAdvisoryConnection!`](objects.md#securityadvisoryconnection)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `after` | [`String`](scalars.md#string) | Returns the elements in the list that come after the specified cursor. |  |
| `before` | [`String`](scalars.md#string) | Returns the elements in the list that come before the specified cursor. |  |
| `first` | [`Int`](scalars.md#int) | Returns the first _n_ elements from the list. |  |
| `identifier` | [`SecurityAdvisoryIdentifierFilter`](inputs.md#securityadvisoryidentifierfilter) | Filter advisories by identifier, e.g. GHSA or CVE. |  |
| `last` | [`Int`](scalars.md#int) | Returns the last _n_ elements from the list. |  |
| `orderBy` | [`SecurityAdvisoryOrder`](inputs.md#securityadvisoryorder) | Ordering options for the returned topics. | `{field: UPDATED_AT, direction: DESC}` |
| `publishedSince` | [`DateTime`](scalars.md#datetime) | Filter advisories to those published since a time in the past. |  |
| `updatedSince` | [`DateTime`](scalars.md#datetime) | Filter advisories to those updated since a time in the past. |  |

## securityAdvisory

> Fetch a Security Advisory by its GHSA ID

**Type:** [`SecurityAdvisory`](objects.md#securityadvisory)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `ghsaId` | [`String!`](scalars.md#string) | GitHub Security Advisory ID. |  |

## securityVulnerabilities

> Software Vulnerabilities documented by GitHub Security Advisories

**Type:** [`SecurityVulnerabilityConnection!`](objects.md#securityvulnerabilityconnection)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `after` | [`String`](scalars.md#string) | Returns the elements in the list that come after the specified cursor. |  |
| `before` | [`String`](scalars.md#string) | Returns the elements in the list that come before the specified cursor. |  |
| `ecosystem` | [`SecurityAdvisoryEcosystem`](enums.md#securityadvisoryecosystem) | An ecosystem to filter vulnerabilities by. |  |
| `first` | [`Int`](scalars.md#int) | Returns the first _n_ elements from the list. |  |
| `last` | [`Int`](scalars.md#int) | Returns the last _n_ elements from the list. |  |
| `orderBy` | [`SecurityVulnerabilityOrder`](inputs.md#securityvulnerabilityorder) | Ordering options for the returned topics. | `{field: UPDATED_AT, direction: DESC}` |
| `package` | [`String`](scalars.md#string) | A package name to filter vulnerabilities by. |  |
| `severities` | [`[SecurityAdvisorySeverity!]`](enums.md#securityadvisoryseverity) | A list of severities to filter vulnerabilities by. |  |

## sponsorsListing

_Deprecated_
> Look up a single Sponsors Listing

**Type:** [`SponsorsListing`](objects.md#sponsorslisting)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `slug` | [`String!`](scalars.md#string) | Select the Sponsors listing which matches this slug |  |

## topic

> Look up a topic by name.

**Type:** [`Topic`](objects.md#topic)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `name` | [`String!`](scalars.md#string) | The topic's name. |  |

## user

> Lookup a user by login.

**Type:** [`User`](objects.md#user)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `login` | [`String!`](scalars.md#string) | The user's login. |  |

## viewer

> The currently authenticated user.

**Type:** [`User!`](objects.md#user)

