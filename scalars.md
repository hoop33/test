# Scalars

## <a name="boolean"></a>Boolean

> Represents `true` or `false` values.

## <a name="date"></a>Date

> An ISO-8601 encoded date string.

## <a name="datetime"></a>DateTime

> An ISO-8601 encoded UTC date string.

## <a name="float"></a>Float

> Represents signed double-precision fractional values as specified by [IEEE 754](https://en.wikipedia.org/wiki/IEEE_floating_point).

## <a name="gitobjectid"></a>GitObjectID

> A Git object ID.

## <a name="gitsshremote"></a>GitSSHRemote

> Git SSH string

## <a name="gittimestamp"></a>GitTimestamp

> An ISO-8601 encoded date string. Unlike the DateTime type, GitTimestamp is not converted in UTC.

## <a name="html"></a>HTML

> A string containing HTML code.

## <a name="id"></a>ID

> Represents a unique identifier that is Base64 obfuscated. It is often used to refetch an object or as key for a cache. The ID type appears in a JSON response as a String; however, it is not intended to be human-readable. When expected as an input type, any string (such as `"VXNlci0xMA=="`) or integer (such as `4`) input value will be accepted as an ID.

## <a name="int"></a>Int

> Represents non-fractional signed whole numeric values. Int can represent values between -(2^31) and 2^31 - 1.

## <a name="precisedatetime"></a>PreciseDateTime

> An ISO-8601 encoded UTC date string with millisecond precison.

## <a name="string"></a>String

> Represents textual data as UTF-8 character sequences. This type is most often used by GraphQL to represent free-form human-readable text.

## <a name="uri"></a>URI

> An RFC 3986, RFC 3987, and RFC 6570 (level 4) compliant URI string.

## <a name="x509certificate"></a>X509Certificate

> A valid x509 certificate string

