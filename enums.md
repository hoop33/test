# Enums

## <a name="actionexecutioncapabilitysetting"></a>ActionExecutionCapabilitySetting

> The possible capabilities for action executions setting.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ALL_ACTIONS` | All action executions are enabled. | no |
| `DISABLED` | All action executions are disabled. | no |
| `LOCAL_ACTIONS_ONLY` | Only actions defined within the repo are allowed. | no |
| `NO_POLICY` | Organization administrators action execution capabilities. | no |

## <a name="auditlogorderfield"></a>AuditLogOrderField

> Properties by which Audit Log connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CREATED_AT` | Order audit log entries by timestamp | no |

## <a name="collaboratoraffiliation"></a>CollaboratorAffiliation

> Collaborators affiliation level with a subject.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ALL` | All collaborators the authenticated user can see. | no |
| `DIRECT` | All collaborators with permissions to an organization-owned subject, regardless of organization membership status. | no |
| `OUTSIDE` | All outside collaborators of an organization-owned subject. | no |

## <a name="commentauthorassociation"></a>CommentAuthorAssociation

> A comment author association with repository.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `COLLABORATOR` | Author has been invited to collaborate on the repository. | no |
| `CONTRIBUTOR` | Author has previously committed to the repository. | no |
| `FIRST_TIMER` | Author has not previously committed to GitHub. | no |
| `FIRST_TIME_CONTRIBUTOR` | Author has not previously committed to the repository. | no |
| `MEMBER` | Author is a member of the organization that owns the repository. | no |
| `NONE` | Author has no association with the repository. | no |
| `OWNER` | Author is the owner of the repository. | no |

## <a name="commentcannotupdatereason"></a>CommentCannotUpdateReason

> The possible errors that will prevent a user from updating a comment.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ARCHIVED` | Unable to create comment because repository is archived. | no |
| `DENIED` | You cannot update this comment | no |
| `INSUFFICIENT_ACCESS` | You must be the author or have write access to this repository to update this comment. | no |
| `LOCKED` | Unable to create comment because issue is locked. | no |
| `LOGIN_REQUIRED` | You must be logged in to update this comment. | no |
| `MAINTENANCE` | Repository is under maintenance. | no |
| `VERIFIED_EMAIL_REQUIRED` | At least one email address must be verified to update this comment. | no |

## <a name="commitcontributionorderfield"></a>CommitContributionOrderField

> Properties by which commit contribution connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `COMMIT_COUNT` | Order commit contributions by how many commits they represent. | no |
| `OCCURRED_AT` | Order commit contributions by when they were made. | no |

## <a name="defaultrepositorypermissionfield"></a>DefaultRepositoryPermissionField

> The possible default permissions for repositories.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ADMIN` | Can read, write, and administrate repos by default | no |
| `NONE` | No access | no |
| `READ` | Can read repos by default | no |
| `WRITE` | Can read and write repos by default | no |

## <a name="deploymentorderfield"></a>DeploymentOrderField

> Properties by which deployment connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CREATED_AT` | Order collection by creation time | no |

## <a name="deploymentstate"></a>DeploymentState

> The possible states in which a deployment can be.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ABANDONED` | The pending deployment was not updated after 30 minutes. | no |
| `ACTIVE` | The deployment is currently active. | no |
| `DESTROYED` | An inactive transient deployment. | no |
| `ERROR` | The deployment experienced an error. | no |
| `FAILURE` | The deployment has failed. | no |
| `INACTIVE` | The deployment is inactive. | no |
| `IN_PROGRESS` | The deployment is in progress. | no |
| `PENDING` | The deployment is pending. | no |
| `QUEUED` | The deployment has queued | no |

## <a name="deploymentstatusstate"></a>DeploymentStatusState

> The possible states for a deployment status.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ERROR` | The deployment experienced an error. | no |
| `FAILURE` | The deployment has failed. | no |
| `INACTIVE` | The deployment is inactive. | no |
| `IN_PROGRESS` | The deployment is in progress. | no |
| `PENDING` | The deployment is pending. | no |
| `QUEUED` | The deployment is queued | no |
| `SUCCESS` | The deployment was successful. | no |

## <a name="diffside"></a>DiffSide

> The possible sides of a diff.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `LEFT` | The left side of the diff. | no |
| `RIGHT` | The right side of the diff. | no |

## <a name="enterpriseadministratorinvitationorderfield"></a>EnterpriseAdministratorInvitationOrderField

> Properties by which enterprise administrator invitation connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CREATED_AT` | Order enterprise administrator member invitations by creation time | no |

## <a name="enterpriseadministratorrole"></a>EnterpriseAdministratorRole

> The possible administrator roles in an enterprise account.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `BILLING_MANAGER` | Represents a billing manager of the enterprise account. | no |
| `OWNER` | Represents an owner of the enterprise account. | no |

## <a name="enterprisedefaultrepositorypermissionsettingvalue"></a>EnterpriseDefaultRepositoryPermissionSettingValue

> The possible values for the enterprise default repository permission setting.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ADMIN` | Organization members will be able to clone, pull, push, and add new collaborators to all organization repositories. | no |
| `NONE` | Organization members will only be able to clone and pull public repositories. | no |
| `NO_POLICY` | Organizations in the enterprise choose default repository permissions for their members. | no |
| `READ` | Organization members will be able to clone and pull all organization repositories. | no |
| `WRITE` | Organization members will be able to clone, pull, and push all organization repositories. | no |

## <a name="enterpriseenableddisabledsettingvalue"></a>EnterpriseEnabledDisabledSettingValue

> The possible values for an enabled/disabled enterprise setting.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `DISABLED` | The setting is disabled for organizations in the enterprise. | no |
| `ENABLED` | The setting is enabled for organizations in the enterprise. | no |
| `NO_POLICY` | There is no policy set for organizations in the enterprise. | no |

## <a name="enterpriseenabledsettingvalue"></a>EnterpriseEnabledSettingValue

> The possible values for an enabled/no policy enterprise setting.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ENABLED` | The setting is enabled for organizations in the enterprise. | no |
| `NO_POLICY` | There is no policy set for organizations in the enterprise. | no |

## <a name="enterprisememberorderfield"></a>EnterpriseMemberOrderField

> Properties by which enterprise member connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CREATED_AT` | Order enterprise members by creation time | no |
| `LOGIN` | Order enterprise members by login | no |

## <a name="enterprisememberscancreaterepositoriessettingvalue"></a>EnterpriseMembersCanCreateRepositoriesSettingValue

> The possible values for the enterprise members can create repositories setting.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ALL` | Members will be able to create public and private repositories. | no |
| `DISABLED` | Members will not be able to create public or private repositories. | no |
| `NO_POLICY` | Organization administrators choose whether to allow members to create repositories. | no |
| `PRIVATE` | Members will be able to create only private repositories. | no |
| `PUBLIC` | Members will be able to create only public repositories. | no |

## <a name="enterprisememberscanmakepurchasessettingvalue"></a>EnterpriseMembersCanMakePurchasesSettingValue

> The possible values for the members can make purchases setting.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `DISABLED` | The setting is disabled for organizations in the enterprise. | no |
| `ENABLED` | The setting is enabled for organizations in the enterprise. | no |

## <a name="enterpriseserverinstallationorderfield"></a>EnterpriseServerInstallationOrderField

> Properties by which Enterprise Server installation connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CREATED_AT` | Order Enterprise Server installations by creation time | no |
| `CUSTOMER_NAME` | Order Enterprise Server installations by customer name | no |
| `HOST_NAME` | Order Enterprise Server installations by host name | no |

## <a name="enterpriseserveruseraccountemailorderfield"></a>EnterpriseServerUserAccountEmailOrderField

> Properties by which Enterprise Server user account email connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `EMAIL` | Order emails by email | no |

## <a name="enterpriseserveruseraccountorderfield"></a>EnterpriseServerUserAccountOrderField

> Properties by which Enterprise Server user account connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `LOGIN` | Order user accounts by login | no |
| `REMOTE_CREATED_AT` | Order user accounts by creation time on the Enterprise Server installation | no |

## <a name="enterpriseserveruseraccountsuploadorderfield"></a>EnterpriseServerUserAccountsUploadOrderField

> Properties by which Enterprise Server user accounts upload connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CREATED_AT` | Order user accounts uploads by creation time | no |

## <a name="enterpriseserveruseraccountsuploadsyncstate"></a>EnterpriseServerUserAccountsUploadSyncState

> Synchronization state of the Enterprise Server user accounts upload

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `FAILURE` | The synchronization of the upload failed. | no |
| `PENDING` | The synchronization of the upload is pending. | no |
| `SUCCESS` | The synchronization of the upload succeeded. | no |

## <a name="enterpriseuseraccountmembershiprole"></a>EnterpriseUserAccountMembershipRole

> The possible roles for enterprise membership.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `MEMBER` | The user is a member of the enterprise membership. | no |
| `OWNER` | The user is an owner of the enterprise membership. | no |

## <a name="enterpriseuserdeployment"></a>EnterpriseUserDeployment

> The possible GitHub Enterprise deployments where this user can exist.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CLOUD` | The user is part of a GitHub Enterprise Cloud deployment. | no |
| `SERVER` | The user is part of a GitHub Enterprise Server deployment. | no |

## <a name="fundingplatform"></a>FundingPlatform

> The possible funding platforms for repository funding links.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `COMMUNITY_BRIDGE` | Community Bridge funding platform. | no |
| `CUSTOM` | Custom funding platform. | no |
| `GITHUB` | GitHub funding platform. | no |
| `ISSUEHUNT` | IssueHunt funding platform. | no |
| `KO_FI` | Ko-fi funding platform. | no |
| `LIBERAPAY` | Liberapay funding platform. | no |
| `OPEN_COLLECTIVE` | Open Collective funding platform. | no |
| `OTECHIE` | Otechie funding platform. | no |
| `PATREON` | Patreon funding platform. | no |
| `TIDELIFT` | Tidelift funding platform. | no |

## <a name="gistorderfield"></a>GistOrderField

> Properties by which gist connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CREATED_AT` | Order gists by creation time | no |
| `PUSHED_AT` | Order gists by push time | no |
| `UPDATED_AT` | Order gists by update time | no |

## <a name="gistprivacy"></a>GistPrivacy

> The privacy of a Gist

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ALL` | Gists that are public and secret | no |
| `PUBLIC` | Public | no |
| `SECRET` | Secret | no |

## <a name="gitsignaturestate"></a>GitSignatureState

> The state of a Git signature.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `BAD_CERT` | The signing certificate or its chain could not be verified | no |
| `BAD_EMAIL` | Invalid email used for signing | no |
| `EXPIRED_KEY` | Signing key expired | no |
| `GPGVERIFY_ERROR` | Internal error - the GPG verification service misbehaved | no |
| `GPGVERIFY_UNAVAILABLE` | Internal error - the GPG verification service is unavailable at the moment | no |
| `INVALID` | Invalid signature | no |
| `MALFORMED_SIG` | Malformed signature | no |
| `NOT_SIGNING_KEY` | The usage flags for the key that signed this don't allow signing | no |
| `NO_USER` | Email used for signing not known to GitHub | no |
| `OCSP_ERROR` | Valid siganture, though certificate revocation check failed | no |
| `OCSP_PENDING` | Valid signature, pending certificate revocation checking | no |
| `OCSP_REVOKED` | One or more certificates in chain has been revoked | no |
| `UNKNOWN_KEY` | Key used for signing not known to GitHub | no |
| `UNKNOWN_SIG_TYPE` | Unknown signature type | no |
| `UNSIGNED` | Unsigned | no |
| `UNVERIFIED_EMAIL` | Email used for signing unverified on GitHub | no |
| `VALID` | Valid signature and verified by GitHub | no |

## <a name="identityproviderconfigurationstate"></a>IdentityProviderConfigurationState

> The possible states in which authentication can be configured with an identity provider.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CONFIGURED` | Authentication with an identity provider is configured but not enforced. | no |
| `ENFORCED` | Authentication with an identity provider is configured and enforced. | no |
| `UNCONFIGURED` | Authentication with an identity provider is not configured. | no |

## <a name="ipallowlistenabledsettingvalue"></a>IpAllowListEnabledSettingValue

> The possible values for the IP allow list enabled setting.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `DISABLED` | The setting is disabled for the owner. | no |
| `ENABLED` | The setting is enabled for the owner. | no |

## <a name="ipallowlistentryorderfield"></a>IpAllowListEntryOrderField

> Properties by which IP allow list entry connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ALLOW_LIST_VALUE` | Order IP allow list entries by the allow list value. | no |
| `CREATED_AT` | Order IP allow list entries by creation time. | no |

## <a name="issueorderfield"></a>IssueOrderField

> Properties by which issue connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `COMMENTS` | Order issues by comment count | no |
| `CREATED_AT` | Order issues by creation time | no |
| `UPDATED_AT` | Order issues by update time | no |

## <a name="issuestate"></a>IssueState

> The possible states of an issue.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CLOSED` | An issue that has been closed | no |
| `OPEN` | An issue that is still open | no |

## <a name="issuetimelineitemsitemtype"></a>IssueTimelineItemsItemType

> The possible item types found in a timeline.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ADDED_TO_PROJECT_EVENT` | Represents a 'added_to_project' event on a given issue or pull request. | no |
| `ASSIGNED_EVENT` | Represents an 'assigned' event on any assignable object. | no |
| `CLOSED_EVENT` | Represents a 'closed' event on any `Closable`. | no |
| `COMMENT_DELETED_EVENT` | Represents a 'comment_deleted' event on a given issue or pull request. | no |
| `CONNECTED_EVENT` | Represents a 'connected' event on a given issue or pull request. | no |
| `CONVERTED_NOTE_TO_ISSUE_EVENT` | Represents a 'converted_note_to_issue' event on a given issue or pull request. | no |
| `CROSS_REFERENCED_EVENT` | Represents a mention made by one issue or pull request to another. | no |
| `DEMILESTONED_EVENT` | Represents a 'demilestoned' event on a given issue or pull request. | no |
| `DISCONNECTED_EVENT` | Represents a 'disconnected' event on a given issue or pull request. | no |
| `ISSUE_COMMENT` | Represents a comment on an Issue. | no |
| `LABELED_EVENT` | Represents a 'labeled' event on a given issue or pull request. | no |
| `LOCKED_EVENT` | Represents a 'locked' event on a given issue or pull request. | no |
| `MARKED_AS_DUPLICATE_EVENT` | Represents a 'marked_as_duplicate' event on a given issue or pull request. | no |
| `MENTIONED_EVENT` | Represents a 'mentioned' event on a given issue or pull request. | no |
| `MILESTONED_EVENT` | Represents a 'milestoned' event on a given issue or pull request. | no |
| `MOVED_COLUMNS_IN_PROJECT_EVENT` | Represents a 'moved_columns_in_project' event on a given issue or pull request. | no |
| `PINNED_EVENT` | Represents a 'pinned' event on a given issue or pull request. | no |
| `REFERENCED_EVENT` | Represents a 'referenced' event on a given `ReferencedSubject`. | no |
| `REMOVED_FROM_PROJECT_EVENT` | Represents a 'removed_from_project' event on a given issue or pull request. | no |
| `RENAMED_TITLE_EVENT` | Represents a 'renamed' event on a given issue or pull request | no |
| `REOPENED_EVENT` | Represents a 'reopened' event on any `Closable`. | no |
| `SUBSCRIBED_EVENT` | Represents a 'subscribed' event on a given `Subscribable`. | no |
| `TRANSFERRED_EVENT` | Represents a 'transferred' event on a given issue or pull request. | no |
| `UNASSIGNED_EVENT` | Represents an 'unassigned' event on any assignable object. | no |
| `UNLABELED_EVENT` | Represents an 'unlabeled' event on a given issue or pull request. | no |
| `UNLOCKED_EVENT` | Represents an 'unlocked' event on a given issue or pull request. | no |
| `UNMARKED_AS_DUPLICATE_EVENT` | Represents an 'unmarked_as_duplicate' event on a given issue or pull request. | no |
| `UNPINNED_EVENT` | Represents an 'unpinned' event on a given issue or pull request. | no |
| `UNSUBSCRIBED_EVENT` | Represents an 'unsubscribed' event on a given `Subscribable`. | no |
| `USER_BLOCKED_EVENT` | Represents a 'user_blocked' event on a given user. | no |

## <a name="labelorderfield"></a>LabelOrderField

> Properties by which label connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CREATED_AT` | Order labels by creation time | no |
| `NAME` | Order labels by name | no |

## <a name="languageorderfield"></a>LanguageOrderField

> Properties by which language connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `SIZE` | Order languages by the size of all files containing the language | no |

## <a name="lockreason"></a>LockReason

> The possible reasons that an issue or pull request was locked.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `OFF_TOPIC` | The issue or pull request was locked because the conversation was off-topic. | no |
| `RESOLVED` | The issue or pull request was locked because the conversation was resolved. | no |
| `SPAM` | The issue or pull request was locked because the conversation was spam. | no |
| `TOO_HEATED` | The issue or pull request was locked because the conversation was too heated. | no |

## <a name="mergeablestate"></a>MergeableState

> Whether or not a PullRequest can be merged.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CONFLICTING` | The pull request cannot be merged due to merge conflicts. | no |
| `MERGEABLE` | The pull request can be merged. | no |
| `UNKNOWN` | The mergeability of the pull request is still being calculated. | no |

## <a name="milestoneorderfield"></a>MilestoneOrderField

> Properties by which milestone connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CREATED_AT` | Order milestones by when they were created. | no |
| `DUE_DATE` | Order milestones by when they are due. | no |
| `NUMBER` | Order milestones by their number. | no |
| `UPDATED_AT` | Order milestones by when they were last updated. | no |

## <a name="milestonestate"></a>MilestoneState

> The possible states of a milestone.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CLOSED` | A milestone that has been closed. | no |
| `OPEN` | A milestone that is still open. | no |

## <a name="oauthapplicationcreateauditentrystate"></a>OauthApplicationCreateAuditEntryState

> The state of an OAuth Application when it was created.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ACTIVE` | The OAuth Application was active and allowed to have OAuth Accesses. | no |
| `PENDING_DELETION` | The OAuth Application was in the process of being deleted. | no |
| `SUSPENDED` | The OAuth Application was suspended from generating OAuth Accesses due to abuse or security concerns. | no |

## <a name="operationtype"></a>OperationType

> The corresponding operation type for the action

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ACCESS` | An existing resource was accessed | no |
| `AUTHENTICATION` | A resource performed an authentication event | no |
| `CREATE` | A new resource was created | no |
| `MODIFY` | An existing resource was modified | no |
| `REMOVE` | An existing resource was removed | no |
| `RESTORE` | An existing resource was restored | no |
| `TRANSFER` | An existing resource was transferred between multiple resources | no |

## <a name="orderdirection"></a>OrderDirection

> Possible directions in which to order a list of items when provided an `orderBy` argument.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ASC` | Specifies an ascending order for a given `orderBy` argument. | no |
| `DESC` | Specifies a descending order for a given `orderBy` argument. | no |

## <a name="orgaddmemberauditentrypermission"></a>OrgAddMemberAuditEntryPermission

> The permissions available to members on an Organization.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ADMIN` | Can read, clone, push, and add collaborators to repositories. | no |
| `READ` | Can read and clone repositories. | no |

## <a name="orgcreateauditentrybillingplan"></a>OrgCreateAuditEntryBillingPlan

> The billing plans available for organizations.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `BUSINESS` | Team Plan | no |
| `BUSINESS_PLUS` | Enterprise Cloud Plan | no |
| `FREE` | Free Plan | no |
| `TIERED_PER_SEAT` | Tiered Per Seat Plan | no |
| `UNLIMITED` | Legacy Unlimited Plan | no |

## <a name="orgremovebillingmanagerauditentryreason"></a>OrgRemoveBillingManagerAuditEntryReason

> The reason a billing manager was removed from an Organization.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `SAML_EXTERNAL_IDENTITY_MISSING` | SAML external identity missing | no |
| `SAML_SSO_ENFORCEMENT_REQUIRES_EXTERNAL_IDENTITY` | SAML SSO enforcement requires an external identity | no |
| `TWO_FACTOR_REQUIREMENT_NON_COMPLIANCE` | The organization required 2FA of its billing managers and this user did not have 2FA enabled. | no |

## <a name="orgremovememberauditentrymembershiptype"></a>OrgRemoveMemberAuditEntryMembershipType

> The type of membership a user has with an Organization.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ADMIN` | Organization administrators have full access and can change several settings, including the names of repositories that belong to the Organization and Owners team membership. In addition, organization admins can delete the organization and all of its repositories. | no |
| `BILLING_MANAGER` | A billing manager is a user who manages the billing settings for the Organization, such as updating payment information. | no |
| `DIRECT_MEMBER` | A direct member is a user that is a member of the Organization. | no |
| `OUTSIDE_COLLABORATOR` | An outside collaborator is a person who isn't explicitly a member of the Organization, but who has Read, Write, or Admin permissions to one or more repositories in the organization. | no |
| `UNAFFILIATED` | An unaffiliated collaborator is a person who is not a member of the Organization and does not have access to any repositories in the Organization. | no |

## <a name="orgremovememberauditentryreason"></a>OrgRemoveMemberAuditEntryReason

> The reason a member was removed from an Organization.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `SAML_EXTERNAL_IDENTITY_MISSING` | SAML external identity missing | no |
| `SAML_SSO_ENFORCEMENT_REQUIRES_EXTERNAL_IDENTITY` | SAML SSO enforcement requires an external identity | no |
| `TWO_FACTOR_ACCOUNT_RECOVERY` | User was removed from organization during account recovery | no |
| `TWO_FACTOR_REQUIREMENT_NON_COMPLIANCE` | The organization required 2FA of its billing managers and this user did not have 2FA enabled. | no |
| `USER_ACCOUNT_DELETED` | User account has been deleted | no |

## <a name="orgremoveoutsidecollaboratorauditentrymembershiptype"></a>OrgRemoveOutsideCollaboratorAuditEntryMembershipType

> The type of membership a user has with an Organization.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `BILLING_MANAGER` | A billing manager is a user who manages the billing settings for the Organization, such as updating payment information. | no |
| `OUTSIDE_COLLABORATOR` | An outside collaborator is a person who isn't explicitly a member of the Organization, but who has Read, Write, or Admin permissions to one or more repositories in the organization. | no |
| `UNAFFILIATED` | An unaffiliated collaborator is a person who is not a member of the Organization and does not have access to any repositories in the organization. | no |

## <a name="orgremoveoutsidecollaboratorauditentryreason"></a>OrgRemoveOutsideCollaboratorAuditEntryReason

> The reason an outside collaborator was removed from an Organization.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `SAML_EXTERNAL_IDENTITY_MISSING` | SAML external identity missing | no |
| `TWO_FACTOR_REQUIREMENT_NON_COMPLIANCE` | The organization required 2FA of its billing managers and this user did not have 2FA enabled. | no |

## <a name="orgupdatedefaultrepositorypermissionauditentrypermission"></a>OrgUpdateDefaultRepositoryPermissionAuditEntryPermission

> The default permission a repository can have in an Organization.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ADMIN` | Can read, clone, push, and add collaborators to repositories. | no |
| `NONE` | No default permission value. | no |
| `READ` | Can read and clone repositories. | no |
| `WRITE` | Can read, clone and push to repositories. | no |

## <a name="orgupdatememberauditentrypermission"></a>OrgUpdateMemberAuditEntryPermission

> The permissions available to members on an Organization.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ADMIN` | Can read, clone, push, and add collaborators to repositories. | no |
| `READ` | Can read and clone repositories. | no |

## <a name="orgupdatememberrepositorycreationpermissionauditentryvisibility"></a>OrgUpdateMemberRepositoryCreationPermissionAuditEntryVisibility

> The permissions available for repository creation on an Organization.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ALL` | All organization members are restricted from creating any repositories. | no |
| `INTERNAL` | All organization members are restricted from creating internal repositories. | no |
| `NONE` | All organization members are allowed to create any repositories. | no |
| `PRIVATE` | All organization members are restricted from creating private repositories. | no |
| `PRIVATE_INTERNAL` | All organization members are restricted from creating private or internal repositories. | no |
| `PUBLIC` | All organization members are restricted from creating public repositories. | no |
| `PUBLIC_INTERNAL` | All organization members are restricted from creating public or internal repositories. | no |
| `PUBLIC_PRIVATE` | All organization members are restricted from creating public or private repositories. | no |

## <a name="organizationinvitationrole"></a>OrganizationInvitationRole

> The possible organization invitation roles.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ADMIN` | The user is invited to be an admin of the organization. | no |
| `BILLING_MANAGER` | The user is invited to be a billing manager of the organization. | no |
| `DIRECT_MEMBER` | The user is invited to be a direct member of the organization. | no |
| `REINSTATE` | The user's previous role will be reinstated. | no |

## <a name="organizationinvitationtype"></a>OrganizationInvitationType

> The possible organization invitation types.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `EMAIL` | The invitation was to an email address. | no |
| `USER` | The invitation was to an existing user. | no |

## <a name="organizationmemberrole"></a>OrganizationMemberRole

> The possible roles within an organization for its members.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ADMIN` | The user is an administrator of the organization. | no |
| `MEMBER` | The user is a member of the organization. | no |

## <a name="organizationmemberscancreaterepositoriessettingvalue"></a>OrganizationMembersCanCreateRepositoriesSettingValue

> The possible values for the members can create repositories setting on an organization.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ALL` | Members will be able to create public and private repositories. | no |
| `DISABLED` | Members will not be able to create public or private repositories. | no |
| `PRIVATE` | Members will be able to create only private repositories. | no |

## <a name="organizationorderfield"></a>OrganizationOrderField

> Properties by which organization connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CREATED_AT` | Order organizations by creation time | no |
| `LOGIN` | Order organizations by login | no |

## <a name="packagefileorderfield"></a>PackageFileOrderField

> Properties by which package file connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CREATED_AT` | Order package files by creation time | no |

## <a name="packageorderfield"></a>PackageOrderField

> Properties by which package connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CREATED_AT` | Order packages by creation time | no |

## <a name="packagetype"></a>PackageType

> The possible types of a package.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `DEBIAN` | A debian package. | no |
| `DOCKER` | A docker image. | no |
| `MAVEN` | A maven package. | no |
| `NPM` | An npm package. | no |
| `NUGET` | A nuget package. | no |
| `PYPI` | A python package. | no |
| `RUBYGEMS` | A rubygems package. | no |

## <a name="packageversionorderfield"></a>PackageVersionOrderField

> Properties by which package version connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CREATED_AT` | Order package versions by creation time | no |

## <a name="pinnableitemtype"></a>PinnableItemType

> Represents items that can be pinned to a profile page or dashboard.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `GIST` | A gist. | no |
| `ISSUE` | An issue. | no |
| `ORGANIZATION` | An organization. | no |
| `PROJECT` | A project. | no |
| `PULL_REQUEST` | A pull request. | no |
| `REPOSITORY` | A repository. | no |
| `TEAM` | A team. | no |
| `USER` | A user. | no |

## <a name="projectcardarchivedstate"></a>ProjectCardArchivedState

> The possible archived states of a project card.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ARCHIVED` | A project card that is archived | no |
| `NOT_ARCHIVED` | A project card that is not archived | no |

## <a name="projectcardstate"></a>ProjectCardState

> Various content states of a ProjectCard

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CONTENT_ONLY` | The card has content only. | no |
| `NOTE_ONLY` | The card has a note only. | no |
| `REDACTED` | The card is redacted. | no |

## <a name="projectcolumnpurpose"></a>ProjectColumnPurpose

> The semantic purpose of the column - todo, in progress, or done.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `DONE` | The column contains cards which are complete | no |
| `IN_PROGRESS` | The column contains cards which are currently being worked on | no |
| `TODO` | The column contains cards still to be worked on | no |

## <a name="projectorderfield"></a>ProjectOrderField

> Properties by which project connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CREATED_AT` | Order projects by creation time | no |
| `NAME` | Order projects by name | no |
| `UPDATED_AT` | Order projects by update time | no |

## <a name="projectstate"></a>ProjectState

> State of the project; either 'open' or 'closed'

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CLOSED` | The project is closed. | no |
| `OPEN` | The project is open. | no |

## <a name="projecttemplate"></a>ProjectTemplate

> GitHub-provided templates for Projects

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `AUTOMATED_KANBAN_V2` | Create a board with v2 triggers to automatically move cards across To do, In progress and Done columns. | no |
| `AUTOMATED_REVIEWS_KANBAN` | Create a board with triggers to automatically move cards across columns with review automation. | no |
| `BASIC_KANBAN` | Create a board with columns for To do, In progress and Done. | no |
| `BUG_TRIAGE` | Create a board to triage and prioritize bugs with To do, priority, and Done columns. | no |

## <a name="pullrequestmergemethod"></a>PullRequestMergeMethod

> Represents available types of methods to use when merging a pull request.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `MERGE` | Add all commits from the head branch to the base branch with a merge commit. | no |
| `REBASE` | Add all commits from the head branch onto the base branch individually. | no |
| `SQUASH` | Combine all commits from the head branch into a single commit in the base branch. | no |

## <a name="pullrequestorderfield"></a>PullRequestOrderField

> Properties by which pull_requests connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CREATED_AT` | Order pull_requests by creation time | no |
| `UPDATED_AT` | Order pull_requests by update time | no |

## <a name="pullrequestreviewcommentstate"></a>PullRequestReviewCommentState

> The possible states of a pull request review comment.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `PENDING` | A comment that is part of a pending review | no |
| `SUBMITTED` | A comment that is part of a submitted review | no |

## <a name="pullrequestreviewdecision"></a>PullRequestReviewDecision

> The review status of a pull request.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `APPROVED` | The pull request has received an approving review. | no |
| `CHANGES_REQUESTED` | Changes have been requested on the pull request. | no |
| `REVIEW_REQUIRED` | A review is required before the pull request can be merged. | no |

## <a name="pullrequestreviewevent"></a>PullRequestReviewEvent

> The possible events to perform on a pull request review.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `APPROVE` | Submit feedback and approve merging these changes. | no |
| `COMMENT` | Submit general feedback without explicit approval. | no |
| `DISMISS` | Dismiss review so it now longer effects merging. | no |
| `REQUEST_CHANGES` | Submit feedback that must be addressed before merging. | no |

## <a name="pullrequestreviewstate"></a>PullRequestReviewState

> The possible states of a pull request review.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `APPROVED` | A review allowing the pull request to merge. | no |
| `CHANGES_REQUESTED` | A review blocking the pull request from merging. | no |
| `COMMENTED` | An informational review. | no |
| `DISMISSED` | A review that has been dismissed. | no |
| `PENDING` | A review that has not yet been submitted. | no |

## <a name="pullrequeststate"></a>PullRequestState

> The possible states of a pull request.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CLOSED` | A pull request that has been closed without being merged. | no |
| `MERGED` | A pull request that has been closed by being merged. | no |
| `OPEN` | A pull request that is still open. | no |

## <a name="pullrequesttimelineitemsitemtype"></a>PullRequestTimelineItemsItemType

> The possible item types found in a timeline.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ADDED_TO_PROJECT_EVENT` | Represents a 'added_to_project' event on a given issue or pull request. | no |
| `ASSIGNED_EVENT` | Represents an 'assigned' event on any assignable object. | no |
| `AUTOMATIC_BASE_CHANGE_FAILED_EVENT` | Represents a 'automatic_base_change_failed' event on a given pull request. | no |
| `AUTOMATIC_BASE_CHANGE_SUCCEEDED_EVENT` | Represents a 'automatic_base_change_succeeded' event on a given pull request. | no |
| `BASE_REF_CHANGED_EVENT` | Represents a 'base_ref_changed' event on a given issue or pull request. | no |
| `BASE_REF_FORCE_PUSHED_EVENT` | Represents a 'base_ref_force_pushed' event on a given pull request. | no |
| `CLOSED_EVENT` | Represents a 'closed' event on any `Closable`. | no |
| `COMMENT_DELETED_EVENT` | Represents a 'comment_deleted' event on a given issue or pull request. | no |
| `CONNECTED_EVENT` | Represents a 'connected' event on a given issue or pull request. | no |
| `CONVERTED_NOTE_TO_ISSUE_EVENT` | Represents a 'converted_note_to_issue' event on a given issue or pull request. | no |
| `CONVERT_TO_DRAFT_EVENT` | Represents a 'convert_to_draft' event on a given pull request. | no |
| `CROSS_REFERENCED_EVENT` | Represents a mention made by one issue or pull request to another. | no |
| `DEMILESTONED_EVENT` | Represents a 'demilestoned' event on a given issue or pull request. | no |
| `DEPLOYED_EVENT` | Represents a 'deployed' event on a given pull request. | no |
| `DEPLOYMENT_ENVIRONMENT_CHANGED_EVENT` | Represents a 'deployment_environment_changed' event on a given pull request. | no |
| `DISCONNECTED_EVENT` | Represents a 'disconnected' event on a given issue or pull request. | no |
| `HEAD_REF_DELETED_EVENT` | Represents a 'head_ref_deleted' event on a given pull request. | no |
| `HEAD_REF_FORCE_PUSHED_EVENT` | Represents a 'head_ref_force_pushed' event on a given pull request. | no |
| `HEAD_REF_RESTORED_EVENT` | Represents a 'head_ref_restored' event on a given pull request. | no |
| `ISSUE_COMMENT` | Represents a comment on an Issue. | no |
| `LABELED_EVENT` | Represents a 'labeled' event on a given issue or pull request. | no |
| `LOCKED_EVENT` | Represents a 'locked' event on a given issue or pull request. | no |
| `MARKED_AS_DUPLICATE_EVENT` | Represents a 'marked_as_duplicate' event on a given issue or pull request. | no |
| `MENTIONED_EVENT` | Represents a 'mentioned' event on a given issue or pull request. | no |
| `MERGED_EVENT` | Represents a 'merged' event on a given pull request. | no |
| `MILESTONED_EVENT` | Represents a 'milestoned' event on a given issue or pull request. | no |
| `MOVED_COLUMNS_IN_PROJECT_EVENT` | Represents a 'moved_columns_in_project' event on a given issue or pull request. | no |
| `PINNED_EVENT` | Represents a 'pinned' event on a given issue or pull request. | no |
| `PULL_REQUEST_COMMIT` | Represents a Git commit part of a pull request. | no |
| `PULL_REQUEST_COMMIT_COMMENT_THREAD` | Represents a commit comment thread part of a pull request. | no |
| `PULL_REQUEST_REVIEW` | A review object for a given pull request. | no |
| `PULL_REQUEST_REVIEW_THREAD` | A threaded list of comments for a given pull request. | no |
| `PULL_REQUEST_REVISION_MARKER` | Represents the latest point in the pull request timeline for which the viewer has seen the pull request's commits. | no |
| `READY_FOR_REVIEW_EVENT` | Represents a 'ready_for_review' event on a given pull request. | no |
| `REFERENCED_EVENT` | Represents a 'referenced' event on a given `ReferencedSubject`. | no |
| `REMOVED_FROM_PROJECT_EVENT` | Represents a 'removed_from_project' event on a given issue or pull request. | no |
| `RENAMED_TITLE_EVENT` | Represents a 'renamed' event on a given issue or pull request | no |
| `REOPENED_EVENT` | Represents a 'reopened' event on any `Closable`. | no |
| `REVIEW_DISMISSED_EVENT` | Represents a 'review_dismissed' event on a given issue or pull request. | no |
| `REVIEW_REQUESTED_EVENT` | Represents an 'review_requested' event on a given pull request. | no |
| `REVIEW_REQUEST_REMOVED_EVENT` | Represents an 'review_request_removed' event on a given pull request. | no |
| `SUBSCRIBED_EVENT` | Represents a 'subscribed' event on a given `Subscribable`. | no |
| `TRANSFERRED_EVENT` | Represents a 'transferred' event on a given issue or pull request. | no |
| `UNASSIGNED_EVENT` | Represents an 'unassigned' event on any assignable object. | no |
| `UNLABELED_EVENT` | Represents an 'unlabeled' event on a given issue or pull request. | no |
| `UNLOCKED_EVENT` | Represents an 'unlocked' event on a given issue or pull request. | no |
| `UNMARKED_AS_DUPLICATE_EVENT` | Represents an 'unmarked_as_duplicate' event on a given issue or pull request. | no |
| `UNPINNED_EVENT` | Represents an 'unpinned' event on a given issue or pull request. | no |
| `UNSUBSCRIBED_EVENT` | Represents an 'unsubscribed' event on a given `Subscribable`. | no |
| `USER_BLOCKED_EVENT` | Represents a 'user_blocked' event on a given user. | no |

## <a name="pullrequestupdatestate"></a>PullRequestUpdateState

> The possible target states when updating a pull request.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CLOSED` | A pull request that has been closed without being merged. | no |
| `OPEN` | A pull request that is still open. | no |

## <a name="reactioncontent"></a>ReactionContent

> Emojis that can be attached to Issues, Pull Requests and Comments.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CONFUSED` | Represents the `:confused:` emoji. | no |
| `EYES` | Represents the `:eyes:` emoji. | no |
| `HEART` | Represents the `:heart:` emoji. | no |
| `HOORAY` | Represents the `:hooray:` emoji. | no |
| `LAUGH` | Represents the `:laugh:` emoji. | no |
| `ROCKET` | Represents the `:rocket:` emoji. | no |
| `THUMBS_DOWN` | Represents the `:-1:` emoji. | no |
| `THUMBS_UP` | Represents the `:+1:` emoji. | no |

## <a name="reactionorderfield"></a>ReactionOrderField

> A list of fields that reactions can be ordered by.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CREATED_AT` | Allows ordering a list of reactions by when they were created. | no |

## <a name="reforderfield"></a>RefOrderField

> Properties by which ref connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ALPHABETICAL` | Order refs by their alphanumeric name | no |
| `TAG_COMMIT_DATE` | Order refs by underlying commit date if the ref prefix is refs/tags/ | no |

## <a name="releaseorderfield"></a>ReleaseOrderField

> Properties by which release connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CREATED_AT` | Order releases by creation time | no |
| `NAME` | Order releases alphabetically by name | no |

## <a name="repoaccessauditentryvisibility"></a>RepoAccessAuditEntryVisibility

> The privacy of a repository

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `INTERNAL` | The repository is visible only to users in the same business. | no |
| `PRIVATE` | The repository is visible only to those with explicit access. | no |
| `PUBLIC` | The repository is visible to everyone. | no |

## <a name="repoaddmemberauditentryvisibility"></a>RepoAddMemberAuditEntryVisibility

> The privacy of a repository

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `INTERNAL` | The repository is visible only to users in the same business. | no |
| `PRIVATE` | The repository is visible only to those with explicit access. | no |
| `PUBLIC` | The repository is visible to everyone. | no |

## <a name="repoarchivedauditentryvisibility"></a>RepoArchivedAuditEntryVisibility

> The privacy of a repository

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `INTERNAL` | The repository is visible only to users in the same business. | no |
| `PRIVATE` | The repository is visible only to those with explicit access. | no |
| `PUBLIC` | The repository is visible to everyone. | no |

## <a name="repochangemergesettingauditentrymergetype"></a>RepoChangeMergeSettingAuditEntryMergeType

> The merge options available for pull requests to this repository.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `MERGE` | The pull request is added to the base branch in a merge commit. | no |
| `REBASE` | Commits from the pull request are added onto the base branch individually without a merge commit. | no |
| `SQUASH` | The pull request's commits are squashed into a single commit before they are merged to the base branch. | no |

## <a name="repocreateauditentryvisibility"></a>RepoCreateAuditEntryVisibility

> The privacy of a repository

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `INTERNAL` | The repository is visible only to users in the same business. | no |
| `PRIVATE` | The repository is visible only to those with explicit access. | no |
| `PUBLIC` | The repository is visible to everyone. | no |

## <a name="repodestroyauditentryvisibility"></a>RepoDestroyAuditEntryVisibility

> The privacy of a repository

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `INTERNAL` | The repository is visible only to users in the same business. | no |
| `PRIVATE` | The repository is visible only to those with explicit access. | no |
| `PUBLIC` | The repository is visible to everyone. | no |

## <a name="reporemovememberauditentryvisibility"></a>RepoRemoveMemberAuditEntryVisibility

> The privacy of a repository

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `INTERNAL` | The repository is visible only to users in the same business. | no |
| `PRIVATE` | The repository is visible only to those with explicit access. | no |
| `PUBLIC` | The repository is visible to everyone. | no |

## <a name="reportedcontentclassifiers"></a>ReportedContentClassifiers

> The reasons a piece of content can be reported or minimized.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ABUSE` | An abusive or harassing piece of content | no |
| `DUPLICATE` | A duplicated piece of content | no |
| `OFF_TOPIC` | An irrelevant piece of content | no |
| `OUTDATED` | An outdated piece of content | no |
| `RESOLVED` | The content has been resolved | no |
| `SPAM` | A spammy piece of content | no |

## <a name="repositoryaffiliation"></a>RepositoryAffiliation

> The affiliation of a user to a repository

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `COLLABORATOR` | Repositories that the user has been added to as a collaborator. | no |
| `ORGANIZATION_MEMBER` | Repositories that the user has access to through being a member of an organization. This includes every repository on every team that the user is on. | no |
| `OWNER` | Repositories that are owned by the authenticated user. | no |

## <a name="repositorycontributiontype"></a>RepositoryContributionType

> The reason a repository is listed as 'contributed'.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `COMMIT` | Created a commit | no |
| `ISSUE` | Created an issue | no |
| `PULL_REQUEST` | Created a pull request | no |
| `PULL_REQUEST_REVIEW` | Reviewed a pull request | no |
| `REPOSITORY` | Created the repository | no |

## <a name="repositoryinvitationorderfield"></a>RepositoryInvitationOrderField

> Properties by which repository invitation connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CREATED_AT` | Order repository invitations by creation time | no |
| `INVITEE_LOGIN` | Order repository invitations by invitee login | `INVITEE_LOGIN` is no longer a valid field value. Repository invitations can now be associated with an email, not only an invitee. Removal on 2020-10-01 UTC. |

## <a name="repositorylockreason"></a>RepositoryLockReason

> The possible reasons a given repository could be in a locked state.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `BILLING` | The repository is locked due to a billing related reason. | no |
| `MIGRATING` | The repository is locked due to a migration. | no |
| `MOVING` | The repository is locked due to a move. | no |
| `RENAME` | The repository is locked due to a rename. | no |

## <a name="repositoryorderfield"></a>RepositoryOrderField

> Properties by which repository connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CREATED_AT` | Order repositories by creation time | no |
| `NAME` | Order repositories by name | no |
| `PUSHED_AT` | Order repositories by push time | no |
| `STARGAZERS` | Order repositories by number of stargazers | no |
| `UPDATED_AT` | Order repositories by update time | no |

## <a name="repositorypermission"></a>RepositoryPermission

> The access level to a repository

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ADMIN` | Can read, clone, and push to this repository. Can also manage issues, pull requests, and repository settings, including adding collaborators | no |
| `MAINTAIN` | Can read, clone, and push to this repository. They can also manage issues, pull requests, and some repository settings | no |
| `READ` | Can read and clone this repository. Can also open and comment on issues and pull requests | no |
| `TRIAGE` | Can read and clone this repository. Can also manage issues and pull requests | no |
| `WRITE` | Can read, clone, and push to this repository. Can also manage issues and pull requests | no |

## <a name="repositoryprivacy"></a>RepositoryPrivacy

> The privacy of a repository

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `PRIVATE` | Private | no |
| `PUBLIC` | Public | no |

## <a name="repositoryvisibility"></a>RepositoryVisibility

> The repository's visibility level.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `INTERNAL` | The repository is visible only to users in the same business. | no |
| `PRIVATE` | The repository is visible only to those with explicit access. | no |
| `PUBLIC` | The repository is visible to everyone. | no |

## <a name="samldigestalgorithm"></a>SamlDigestAlgorithm

> The possible digest algorithms used to sign SAML requests for an identity provider.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `SHA1` | SHA1 | no |
| `SHA256` | SHA256 | no |
| `SHA384` | SHA384 | no |
| `SHA512` | SHA512 | no |

## <a name="samlsignaturealgorithm"></a>SamlSignatureAlgorithm

> The possible signature algorithms used to sign SAML requests for a Identity Provider.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `RSA_SHA1` | RSA-SHA1 | no |
| `RSA_SHA256` | RSA-SHA256 | no |
| `RSA_SHA384` | RSA-SHA384 | no |
| `RSA_SHA512` | RSA-SHA512 | no |

## <a name="savedreplyorderfield"></a>SavedReplyOrderField

> Properties by which saved reply connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `UPDATED_AT` | Order saved reply by when they were updated. | no |

## <a name="searchtype"></a>SearchType

> Represents the individual results of a search.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ISSUE` | Returns results matching issues in repositories. | no |
| `REPOSITORY` | Returns results matching repositories. | no |
| `USER` | Returns results matching users and organizations on GitHub. | no |

## <a name="securityadvisoryecosystem"></a>SecurityAdvisoryEcosystem

> The possible ecosystems of a security vulnerability's package.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `COMPOSER` | PHP packages hosted at packagist.org | no |
| `MAVEN` | Java artifacts hosted at the Maven central repository | no |
| `NPM` | JavaScript packages hosted at npmjs.com | no |
| `NUGET` | .NET packages hosted at the NuGet Gallery | no |
| `PIP` | Python packages hosted at PyPI.org | no |
| `RUBYGEMS` | Ruby gems hosted at RubyGems.org | no |

## <a name="securityadvisoryidentifiertype"></a>SecurityAdvisoryIdentifierType

> Identifier formats available for advisories.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CVE` | Common Vulnerabilities and Exposures Identifier. | no |
| `GHSA` | GitHub Security Advisory ID. | no |

## <a name="securityadvisoryorderfield"></a>SecurityAdvisoryOrderField

> Properties by which security advisory connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `PUBLISHED_AT` | Order advisories by publication time | no |
| `UPDATED_AT` | Order advisories by update time | no |

## <a name="securityadvisoryseverity"></a>SecurityAdvisorySeverity

> Severity of the vulnerability.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CRITICAL` | Critical. | no |
| `HIGH` | High. | no |
| `LOW` | Low. | no |
| `MODERATE` | Moderate. | no |

## <a name="securityvulnerabilityorderfield"></a>SecurityVulnerabilityOrderField

> Properties by which security vulnerability connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `UPDATED_AT` | Order vulnerability by update time | no |

## <a name="sponsorstierorderfield"></a>SponsorsTierOrderField

> Properties by which Sponsors tiers connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CREATED_AT` | Order tiers by creation time. | no |
| `MONTHLY_PRICE_IN_CENTS` | Order tiers by their monthly price in cents | no |

## <a name="sponsorshiporderfield"></a>SponsorshipOrderField

> Properties by which sponsorship connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CREATED_AT` | Order sponsorship by creation time. | no |

## <a name="sponsorshipprivacy"></a>SponsorshipPrivacy

> The privacy of a sponsorship

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `PRIVATE` | Private | no |
| `PUBLIC` | Public | no |

## <a name="starorderfield"></a>StarOrderField

> Properties by which star connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `STARRED_AT` | Allows ordering a list of stars by when they were created. | no |

## <a name="statusstate"></a>StatusState

> The possible commit status states.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ERROR` | Status is errored. | no |
| `EXPECTED` | Status is expected. | no |
| `FAILURE` | Status is failing. | no |
| `PENDING` | Status is pending. | no |
| `SUCCESS` | Status is successful. | no |

## <a name="subscriptionstate"></a>SubscriptionState

> The possible states of a subscription.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `IGNORED` | The User is never notified. | no |
| `SUBSCRIBED` | The User is notified of all conversations. | no |
| `UNSUBSCRIBED` | The User is only notified when participating or @mentioned. | no |

## <a name="teamdiscussioncommentorderfield"></a>TeamDiscussionCommentOrderField

> Properties by which team discussion comment connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `NUMBER` | Allows sequential ordering of team discussion comments (which is equivalent to chronological ordering). | no |

## <a name="teamdiscussionorderfield"></a>TeamDiscussionOrderField

> Properties by which team discussion connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CREATED_AT` | Allows chronological ordering of team discussions. | no |

## <a name="teammemberorderfield"></a>TeamMemberOrderField

> Properties by which team member connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CREATED_AT` | Order team members by creation time | no |
| `LOGIN` | Order team members by login | no |

## <a name="teammemberrole"></a>TeamMemberRole

> The possible team member roles; either 'maintainer' or 'member'.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `MAINTAINER` | A team maintainer has permission to add and remove team members. | no |
| `MEMBER` | A team member has no administrative permissions on the team. | no |

## <a name="teammembershiptype"></a>TeamMembershipType

> Defines which types of team members are included in the returned list. Can be one of IMMEDIATE, CHILD_TEAM or ALL.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ALL` | Includes immediate and child team members for the team. | no |
| `CHILD_TEAM` | Includes only child team members for the team. | no |
| `IMMEDIATE` | Includes only immediate members of the team. | no |

## <a name="teamorderfield"></a>TeamOrderField

> Properties by which team connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `NAME` | Allows ordering a list of teams by name. | no |

## <a name="teamprivacy"></a>TeamPrivacy

> The possible team privacy values.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `SECRET` | A secret team can only be seen by its members. | no |
| `VISIBLE` | A visible team can be seen and @mentioned by every member of the organization. | no |

## <a name="teamrepositoryorderfield"></a>TeamRepositoryOrderField

> Properties by which team repository connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `CREATED_AT` | Order repositories by creation time | no |
| `NAME` | Order repositories by name | no |
| `PERMISSION` | Order repositories by permission | no |
| `PUSHED_AT` | Order repositories by push time | no |
| `STARGAZERS` | Order repositories by number of stargazers | no |
| `UPDATED_AT` | Order repositories by update time | no |

## <a name="teamrole"></a>TeamRole

> The role of a user on a team.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ADMIN` | User has admin rights on the team. | no |
| `MEMBER` | User is a member of the team. | no |

## <a name="topicsuggestiondeclinereason"></a>TopicSuggestionDeclineReason

> Reason that the suggested topic is declined.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `NOT_RELEVANT` | The suggested topic is not relevant to the repository. | no |
| `PERSONAL_PREFERENCE` | The viewer does not like the suggested topic. | no |
| `TOO_GENERAL` | The suggested topic is too general for the repository. | no |
| `TOO_SPECIFIC` | The suggested topic is too specific for the repository (e.g. #ruby-on-rails-version-4-2-1). | no |

## <a name="userblockduration"></a>UserBlockDuration

> The possible durations that a user can be blocked for.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ONE_DAY` | The user was blocked for 1 day | no |
| `ONE_MONTH` | The user was blocked for 30 days | no |
| `ONE_WEEK` | The user was blocked for 7 days | no |
| `PERMANENT` | The user was blocked permanently | no |
| `THREE_DAYS` | The user was blocked for 3 days | no |

## <a name="userstatusorderfield"></a>UserStatusOrderField

> Properties by which user status connections can be ordered.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `UPDATED_AT` | Order user statuses by when they were updated. | no |

## <a name="__directivelocation"></a>__DirectiveLocation

> A Directive can be adjacent to many parts of the GraphQL language, a __DirectiveLocation describes one such possible adjacencies.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ARGUMENT_DEFINITION` | Location adjacent to an argument definition. | no |
| `ENUM` | Location adjacent to an enum definition. | no |
| `ENUM_VALUE` | Location adjacent to an enum value definition. | no |
| `FIELD` | Location adjacent to a field. | no |
| `FIELD_DEFINITION` | Location adjacent to a field definition. | no |
| `FRAGMENT_DEFINITION` | Location adjacent to a fragment definition. | no |
| `FRAGMENT_SPREAD` | Location adjacent to a fragment spread. | no |
| `INLINE_FRAGMENT` | Location adjacent to an inline fragment. | no |
| `INPUT_FIELD_DEFINITION` | Location adjacent to an input object field definition. | no |
| `INPUT_OBJECT` | Location adjacent to an input object type definition. | no |
| `INTERFACE` | Location adjacent to an interface definition. | no |
| `MUTATION` | Location adjacent to a mutation operation. | no |
| `OBJECT` | Location adjacent to an object type definition. | no |
| `QUERY` | Location adjacent to a query operation. | no |
| `SCALAR` | Location adjacent to a scalar definition. | no |
| `SCHEMA` | Location adjacent to a schema definition. | no |
| `SUBSCRIPTION` | Location adjacent to a subscription operation. | no |
| `UNION` | Location adjacent to a union definition. | no |

## <a name="__typekind"></a>__TypeKind

> An enum describing what kind of type a given `__Type` is.

### Values

| Name | Description | Deprecated |
| --- | --- | --- |
| `ENUM` | Indicates this type is an enum. `enumValues` is a valid field. | no |
| `INPUT_OBJECT` | Indicates this type is an input object. `inputFields` is a valid field. | no |
| `INTERFACE` | Indicates this type is an interface. `fields` and `possibleTypes` are valid fields. | no |
| `LIST` | Indicates this type is a list. `ofType` is a valid field. | no |
| `NON_NULL` | Indicates this type is a non-null. `ofType` is a valid field. | no |
| `OBJECT` | Indicates this type is an object. `fields` and `interfaces` are valid fields. | no |
| `SCALAR` | Indicates this type is a scalar. | no |
| `UNION` | Indicates this type is a union. `possibleTypes` is a valid field. | no |

