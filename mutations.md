# Mutation

> The root query for implementing GraphQL mutations.

## acceptEnterpriseAdministratorInvitation

> Accepts a pending invitation for a user to become an administrator of an enterprise.

**Type:** [`AcceptEnterpriseAdministratorInvitationPayload`](objects.md#acceptenterpriseadministratorinvitationpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`AcceptEnterpriseAdministratorInvitationInput!`](inputs.md#acceptenterpriseadministratorinvitationinput) |  |  |

## acceptTopicSuggestion

> Applies a suggested topic to the repository.

**Type:** [`AcceptTopicSuggestionPayload`](objects.md#accepttopicsuggestionpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`AcceptTopicSuggestionInput!`](inputs.md#accepttopicsuggestioninput) |  |  |

## addAssigneesToAssignable

> Adds assignees to an assignable object.

**Type:** [`AddAssigneesToAssignablePayload`](objects.md#addassigneestoassignablepayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`AddAssigneesToAssignableInput!`](inputs.md#addassigneestoassignableinput) |  |  |

## addComment

> Adds a comment to an Issue or Pull Request.

**Type:** [`AddCommentPayload`](objects.md#addcommentpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`AddCommentInput!`](inputs.md#addcommentinput) |  |  |

## addLabelsToLabelable

> Adds labels to a labelable object.

**Type:** [`AddLabelsToLabelablePayload`](objects.md#addlabelstolabelablepayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`AddLabelsToLabelableInput!`](inputs.md#addlabelstolabelableinput) |  |  |

## addProjectCard

> Adds a card to a ProjectColumn. Either `contentId` or `note` must be provided but **not** both.

**Type:** [`AddProjectCardPayload`](objects.md#addprojectcardpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`AddProjectCardInput!`](inputs.md#addprojectcardinput) |  |  |

## addProjectColumn

> Adds a column to a Project.

**Type:** [`AddProjectColumnPayload`](objects.md#addprojectcolumnpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`AddProjectColumnInput!`](inputs.md#addprojectcolumninput) |  |  |

## addPullRequestReview

> Adds a review to a Pull Request.

**Type:** [`AddPullRequestReviewPayload`](objects.md#addpullrequestreviewpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`AddPullRequestReviewInput!`](inputs.md#addpullrequestreviewinput) |  |  |

## addPullRequestReviewComment

> Adds a comment to a review.

**Type:** [`AddPullRequestReviewCommentPayload`](objects.md#addpullrequestreviewcommentpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`AddPullRequestReviewCommentInput!`](inputs.md#addpullrequestreviewcommentinput) |  |  |

## addPullRequestReviewThread

> Adds a new thread to a pending Pull Request Review.

**Type:** [`AddPullRequestReviewThreadPayload`](objects.md#addpullrequestreviewthreadpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`AddPullRequestReviewThreadInput!`](inputs.md#addpullrequestreviewthreadinput) |  |  |

## addReaction

> Adds a reaction to a subject.

**Type:** [`AddReactionPayload`](objects.md#addreactionpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`AddReactionInput!`](inputs.md#addreactioninput) |  |  |

## addStar

> Adds a star to a Starrable.

**Type:** [`AddStarPayload`](objects.md#addstarpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`AddStarInput!`](inputs.md#addstarinput) |  |  |

## archiveRepository

> Marks a repository as archived.

**Type:** [`ArchiveRepositoryPayload`](objects.md#archiverepositorypayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`ArchiveRepositoryInput!`](inputs.md#archiverepositoryinput) |  |  |

## cancelEnterpriseAdminInvitation

> Cancels a pending invitation for an administrator to join an enterprise.

**Type:** [`CancelEnterpriseAdminInvitationPayload`](objects.md#cancelenterpriseadmininvitationpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`CancelEnterpriseAdminInvitationInput!`](inputs.md#cancelenterpriseadmininvitationinput) |  |  |

## changeUserStatus

> Update your status on GitHub.

**Type:** [`ChangeUserStatusPayload`](objects.md#changeuserstatuspayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`ChangeUserStatusInput!`](inputs.md#changeuserstatusinput) |  |  |

## clearLabelsFromLabelable

> Clears all labels from a labelable object.

**Type:** [`ClearLabelsFromLabelablePayload`](objects.md#clearlabelsfromlabelablepayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`ClearLabelsFromLabelableInput!`](inputs.md#clearlabelsfromlabelableinput) |  |  |

## cloneProject

> Creates a new project by cloning configuration from an existing project.

**Type:** [`CloneProjectPayload`](objects.md#cloneprojectpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`CloneProjectInput!`](inputs.md#cloneprojectinput) |  |  |

## cloneTemplateRepository

> Create a new repository with the same files and directory structure as a template repository.

**Type:** [`CloneTemplateRepositoryPayload`](objects.md#clonetemplaterepositorypayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`CloneTemplateRepositoryInput!`](inputs.md#clonetemplaterepositoryinput) |  |  |

## closeIssue

> Close an issue.

**Type:** [`CloseIssuePayload`](objects.md#closeissuepayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`CloseIssueInput!`](inputs.md#closeissueinput) |  |  |

## closePullRequest

> Close a pull request.

**Type:** [`ClosePullRequestPayload`](objects.md#closepullrequestpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`ClosePullRequestInput!`](inputs.md#closepullrequestinput) |  |  |

## convertProjectCardNoteToIssue

> Convert a project note card to one associated with a newly created issue.

**Type:** [`ConvertProjectCardNoteToIssuePayload`](objects.md#convertprojectcardnotetoissuepayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`ConvertProjectCardNoteToIssueInput!`](inputs.md#convertprojectcardnotetoissueinput) |  |  |

## createBranchProtectionRule

> Create a new branch protection rule

**Type:** [`CreateBranchProtectionRulePayload`](objects.md#createbranchprotectionrulepayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`CreateBranchProtectionRuleInput!`](inputs.md#createbranchprotectionruleinput) |  |  |

## createEnterpriseOrganization

> Creates an organization as part of an enterprise account.

**Type:** [`CreateEnterpriseOrganizationPayload`](objects.md#createenterpriseorganizationpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`CreateEnterpriseOrganizationInput!`](inputs.md#createenterpriseorganizationinput) |  |  |

## createIpAllowListEntry

> Creates a new IP allow list entry.

**Type:** [`CreateIpAllowListEntryPayload`](objects.md#createipallowlistentrypayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`CreateIpAllowListEntryInput!`](inputs.md#createipallowlistentryinput) |  |  |

## createIssue

> Creates a new issue.

**Type:** [`CreateIssuePayload`](objects.md#createissuepayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`CreateIssueInput!`](inputs.md#createissueinput) |  |  |

## createProject

> Creates a new project.

**Type:** [`CreateProjectPayload`](objects.md#createprojectpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`CreateProjectInput!`](inputs.md#createprojectinput) |  |  |

## createPullRequest

> Create a new pull request

**Type:** [`CreatePullRequestPayload`](objects.md#createpullrequestpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`CreatePullRequestInput!`](inputs.md#createpullrequestinput) |  |  |

## createRef

> Create a new Git Ref.

**Type:** [`CreateRefPayload`](objects.md#createrefpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`CreateRefInput!`](inputs.md#createrefinput) |  |  |

## createRepository

> Create a new repository.

**Type:** [`CreateRepositoryPayload`](objects.md#createrepositorypayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`CreateRepositoryInput!`](inputs.md#createrepositoryinput) |  |  |

## createTeamDiscussion

> Creates a new team discussion.

**Type:** [`CreateTeamDiscussionPayload`](objects.md#createteamdiscussionpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`CreateTeamDiscussionInput!`](inputs.md#createteamdiscussioninput) |  |  |

## createTeamDiscussionComment

> Creates a new team discussion comment.

**Type:** [`CreateTeamDiscussionCommentPayload`](objects.md#createteamdiscussioncommentpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`CreateTeamDiscussionCommentInput!`](inputs.md#createteamdiscussioncommentinput) |  |  |

## declineTopicSuggestion

> Rejects a suggested topic for the repository.

**Type:** [`DeclineTopicSuggestionPayload`](objects.md#declinetopicsuggestionpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`DeclineTopicSuggestionInput!`](inputs.md#declinetopicsuggestioninput) |  |  |

## deleteBranchProtectionRule

> Delete a branch protection rule

**Type:** [`DeleteBranchProtectionRulePayload`](objects.md#deletebranchprotectionrulepayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`DeleteBranchProtectionRuleInput!`](inputs.md#deletebranchprotectionruleinput) |  |  |

## deleteDeployment

> Deletes a deployment.

**Type:** [`DeleteDeploymentPayload`](objects.md#deletedeploymentpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`DeleteDeploymentInput!`](inputs.md#deletedeploymentinput) |  |  |

## deleteIpAllowListEntry

> Deletes an IP allow list entry.

**Type:** [`DeleteIpAllowListEntryPayload`](objects.md#deleteipallowlistentrypayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`DeleteIpAllowListEntryInput!`](inputs.md#deleteipallowlistentryinput) |  |  |

## deleteIssue

> Deletes an Issue object.

**Type:** [`DeleteIssuePayload`](objects.md#deleteissuepayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`DeleteIssueInput!`](inputs.md#deleteissueinput) |  |  |

## deleteIssueComment

> Deletes an IssueComment object.

**Type:** [`DeleteIssueCommentPayload`](objects.md#deleteissuecommentpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`DeleteIssueCommentInput!`](inputs.md#deleteissuecommentinput) |  |  |

## deleteProject

> Deletes a project.

**Type:** [`DeleteProjectPayload`](objects.md#deleteprojectpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`DeleteProjectInput!`](inputs.md#deleteprojectinput) |  |  |

## deleteProjectCard

> Deletes a project card.

**Type:** [`DeleteProjectCardPayload`](objects.md#deleteprojectcardpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`DeleteProjectCardInput!`](inputs.md#deleteprojectcardinput) |  |  |

## deleteProjectColumn

> Deletes a project column.

**Type:** [`DeleteProjectColumnPayload`](objects.md#deleteprojectcolumnpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`DeleteProjectColumnInput!`](inputs.md#deleteprojectcolumninput) |  |  |

## deletePullRequestReview

> Deletes a pull request review.

**Type:** [`DeletePullRequestReviewPayload`](objects.md#deletepullrequestreviewpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`DeletePullRequestReviewInput!`](inputs.md#deletepullrequestreviewinput) |  |  |

## deletePullRequestReviewComment

> Deletes a pull request review comment.

**Type:** [`DeletePullRequestReviewCommentPayload`](objects.md#deletepullrequestreviewcommentpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`DeletePullRequestReviewCommentInput!`](inputs.md#deletepullrequestreviewcommentinput) |  |  |

## deleteRef

> Delete a Git Ref.

**Type:** [`DeleteRefPayload`](objects.md#deleterefpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`DeleteRefInput!`](inputs.md#deleterefinput) |  |  |

## deleteTeamDiscussion

> Deletes a team discussion.

**Type:** [`DeleteTeamDiscussionPayload`](objects.md#deleteteamdiscussionpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`DeleteTeamDiscussionInput!`](inputs.md#deleteteamdiscussioninput) |  |  |

## deleteTeamDiscussionComment

> Deletes a team discussion comment.

**Type:** [`DeleteTeamDiscussionCommentPayload`](objects.md#deleteteamdiscussioncommentpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`DeleteTeamDiscussionCommentInput!`](inputs.md#deleteteamdiscussioncommentinput) |  |  |

## dismissPullRequestReview

> Dismisses an approved or rejected pull request review.

**Type:** [`DismissPullRequestReviewPayload`](objects.md#dismisspullrequestreviewpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`DismissPullRequestReviewInput!`](inputs.md#dismisspullrequestreviewinput) |  |  |

## followUser

> Follow a user.

**Type:** [`FollowUserPayload`](objects.md#followuserpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`FollowUserInput!`](inputs.md#followuserinput) |  |  |

## inviteEnterpriseAdmin

> Invite someone to become an administrator of the enterprise.

**Type:** [`InviteEnterpriseAdminPayload`](objects.md#inviteenterpriseadminpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`InviteEnterpriseAdminInput!`](inputs.md#inviteenterpriseadmininput) |  |  |

## linkRepositoryToProject

> Creates a repository link for a project.

**Type:** [`LinkRepositoryToProjectPayload`](objects.md#linkrepositorytoprojectpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`LinkRepositoryToProjectInput!`](inputs.md#linkrepositorytoprojectinput) |  |  |

## lockLockable

> Lock a lockable object

**Type:** [`LockLockablePayload`](objects.md#locklockablepayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`LockLockableInput!`](inputs.md#locklockableinput) |  |  |

## markPullRequestReadyForReview

> Marks a pull request ready for review.

**Type:** [`MarkPullRequestReadyForReviewPayload`](objects.md#markpullrequestreadyforreviewpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`MarkPullRequestReadyForReviewInput!`](inputs.md#markpullrequestreadyforreviewinput) |  |  |

## mergeBranch

> Merge a head into a branch.

**Type:** [`MergeBranchPayload`](objects.md#mergebranchpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`MergeBranchInput!`](inputs.md#mergebranchinput) |  |  |

## mergePullRequest

> Merge a pull request.

**Type:** [`MergePullRequestPayload`](objects.md#mergepullrequestpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`MergePullRequestInput!`](inputs.md#mergepullrequestinput) |  |  |

## minimizeComment

> Minimizes a comment on an Issue, Commit, Pull Request, or Gist

**Type:** [`MinimizeCommentPayload`](objects.md#minimizecommentpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`MinimizeCommentInput!`](inputs.md#minimizecommentinput) |  |  |

## moveProjectCard

> Moves a project card to another place.

**Type:** [`MoveProjectCardPayload`](objects.md#moveprojectcardpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`MoveProjectCardInput!`](inputs.md#moveprojectcardinput) |  |  |

## moveProjectColumn

> Moves a project column to another place.

**Type:** [`MoveProjectColumnPayload`](objects.md#moveprojectcolumnpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`MoveProjectColumnInput!`](inputs.md#moveprojectcolumninput) |  |  |

## regenerateEnterpriseIdentityProviderRecoveryCodes

> Regenerates the identity provider recovery codes for an enterprise

**Type:** [`RegenerateEnterpriseIdentityProviderRecoveryCodesPayload`](objects.md#regenerateenterpriseidentityproviderrecoverycodespayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`RegenerateEnterpriseIdentityProviderRecoveryCodesInput!`](inputs.md#regenerateenterpriseidentityproviderrecoverycodesinput) |  |  |

## removeAssigneesFromAssignable

> Removes assignees from an assignable object.

**Type:** [`RemoveAssigneesFromAssignablePayload`](objects.md#removeassigneesfromassignablepayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`RemoveAssigneesFromAssignableInput!`](inputs.md#removeassigneesfromassignableinput) |  |  |

## removeEnterpriseAdmin

> Removes an administrator from the enterprise.

**Type:** [`RemoveEnterpriseAdminPayload`](objects.md#removeenterpriseadminpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`RemoveEnterpriseAdminInput!`](inputs.md#removeenterpriseadmininput) |  |  |

## removeEnterpriseIdentityProvider

> Removes the identity provider from an enterprise

**Type:** [`RemoveEnterpriseIdentityProviderPayload`](objects.md#removeenterpriseidentityproviderpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`RemoveEnterpriseIdentityProviderInput!`](inputs.md#removeenterpriseidentityproviderinput) |  |  |

## removeEnterpriseOrganization

> Removes an organization from the enterprise

**Type:** [`RemoveEnterpriseOrganizationPayload`](objects.md#removeenterpriseorganizationpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`RemoveEnterpriseOrganizationInput!`](inputs.md#removeenterpriseorganizationinput) |  |  |

## removeLabelsFromLabelable

> Removes labels from a Labelable object.

**Type:** [`RemoveLabelsFromLabelablePayload`](objects.md#removelabelsfromlabelablepayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`RemoveLabelsFromLabelableInput!`](inputs.md#removelabelsfromlabelableinput) |  |  |

## removeOutsideCollaborator

> Removes outside collaborator from all repositories in an organization.

**Type:** [`RemoveOutsideCollaboratorPayload`](objects.md#removeoutsidecollaboratorpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`RemoveOutsideCollaboratorInput!`](inputs.md#removeoutsidecollaboratorinput) |  |  |

## removeReaction

> Removes a reaction from a subject.

**Type:** [`RemoveReactionPayload`](objects.md#removereactionpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`RemoveReactionInput!`](inputs.md#removereactioninput) |  |  |

## removeStar

> Removes a star from a Starrable.

**Type:** [`RemoveStarPayload`](objects.md#removestarpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`RemoveStarInput!`](inputs.md#removestarinput) |  |  |

## reopenIssue

> Reopen a issue.

**Type:** [`ReopenIssuePayload`](objects.md#reopenissuepayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`ReopenIssueInput!`](inputs.md#reopenissueinput) |  |  |

## reopenPullRequest

> Reopen a pull request.

**Type:** [`ReopenPullRequestPayload`](objects.md#reopenpullrequestpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`ReopenPullRequestInput!`](inputs.md#reopenpullrequestinput) |  |  |

## requestReviews

> Set review requests on a pull request.

**Type:** [`RequestReviewsPayload`](objects.md#requestreviewspayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`RequestReviewsInput!`](inputs.md#requestreviewsinput) |  |  |

## resolveReviewThread

> Marks a review thread as resolved.

**Type:** [`ResolveReviewThreadPayload`](objects.md#resolvereviewthreadpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`ResolveReviewThreadInput!`](inputs.md#resolvereviewthreadinput) |  |  |

## setEnterpriseIdentityProvider

> Creates or updates the identity provider for an enterprise.

**Type:** [`SetEnterpriseIdentityProviderPayload`](objects.md#setenterpriseidentityproviderpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`SetEnterpriseIdentityProviderInput!`](inputs.md#setenterpriseidentityproviderinput) |  |  |

## submitPullRequestReview

> Submits a pending pull request review.

**Type:** [`SubmitPullRequestReviewPayload`](objects.md#submitpullrequestreviewpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`SubmitPullRequestReviewInput!`](inputs.md#submitpullrequestreviewinput) |  |  |

## transferIssue

> Transfer an issue to a different repository

**Type:** [`TransferIssuePayload`](objects.md#transferissuepayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`TransferIssueInput!`](inputs.md#transferissueinput) |  |  |

## unarchiveRepository

> Unarchives a repository.

**Type:** [`UnarchiveRepositoryPayload`](objects.md#unarchiverepositorypayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UnarchiveRepositoryInput!`](inputs.md#unarchiverepositoryinput) |  |  |

## unfollowUser

> Unfollow a user.

**Type:** [`UnfollowUserPayload`](objects.md#unfollowuserpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UnfollowUserInput!`](inputs.md#unfollowuserinput) |  |  |

## unlinkRepositoryFromProject

> Deletes a repository link from a project.

**Type:** [`UnlinkRepositoryFromProjectPayload`](objects.md#unlinkrepositoryfromprojectpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UnlinkRepositoryFromProjectInput!`](inputs.md#unlinkrepositoryfromprojectinput) |  |  |

## unlockLockable

> Unlock a lockable object

**Type:** [`UnlockLockablePayload`](objects.md#unlocklockablepayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UnlockLockableInput!`](inputs.md#unlocklockableinput) |  |  |

## unmarkIssueAsDuplicate

> Unmark an issue as a duplicate of another issue.

**Type:** [`UnmarkIssueAsDuplicatePayload`](objects.md#unmarkissueasduplicatepayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UnmarkIssueAsDuplicateInput!`](inputs.md#unmarkissueasduplicateinput) |  |  |

## unminimizeComment

> Unminimizes a comment on an Issue, Commit, Pull Request, or Gist

**Type:** [`UnminimizeCommentPayload`](objects.md#unminimizecommentpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UnminimizeCommentInput!`](inputs.md#unminimizecommentinput) |  |  |

## unresolveReviewThread

> Marks a review thread as unresolved.

**Type:** [`UnresolveReviewThreadPayload`](objects.md#unresolvereviewthreadpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UnresolveReviewThreadInput!`](inputs.md#unresolvereviewthreadinput) |  |  |

## updateBranchProtectionRule

> Create a new branch protection rule

**Type:** [`UpdateBranchProtectionRulePayload`](objects.md#updatebranchprotectionrulepayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateBranchProtectionRuleInput!`](inputs.md#updatebranchprotectionruleinput) |  |  |

## updateEnterpriseActionExecutionCapabilitySetting

> Sets the action execution capability setting for an enterprise.

**Type:** [`UpdateEnterpriseActionExecutionCapabilitySettingPayload`](objects.md#updateenterpriseactionexecutioncapabilitysettingpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateEnterpriseActionExecutionCapabilitySettingInput!`](inputs.md#updateenterpriseactionexecutioncapabilitysettinginput) |  |  |

## updateEnterpriseAdministratorRole

> Updates the role of an enterprise administrator.

**Type:** [`UpdateEnterpriseAdministratorRolePayload`](objects.md#updateenterpriseadministratorrolepayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateEnterpriseAdministratorRoleInput!`](inputs.md#updateenterpriseadministratorroleinput) |  |  |

## updateEnterpriseAllowPrivateRepositoryForkingSetting

> Sets whether private repository forks are enabled for an enterprise.

**Type:** [`UpdateEnterpriseAllowPrivateRepositoryForkingSettingPayload`](objects.md#updateenterpriseallowprivaterepositoryforkingsettingpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateEnterpriseAllowPrivateRepositoryForkingSettingInput!`](inputs.md#updateenterpriseallowprivaterepositoryforkingsettinginput) |  |  |

## updateEnterpriseDefaultRepositoryPermissionSetting

> Sets the default repository permission for organizations in an enterprise.

**Type:** [`UpdateEnterpriseDefaultRepositoryPermissionSettingPayload`](objects.md#updateenterprisedefaultrepositorypermissionsettingpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateEnterpriseDefaultRepositoryPermissionSettingInput!`](inputs.md#updateenterprisedefaultrepositorypermissionsettinginput) |  |  |

## updateEnterpriseMembersCanChangeRepositoryVisibilitySetting

> Sets whether organization members with admin permissions on a repository can change repository visibility.

**Type:** [`UpdateEnterpriseMembersCanChangeRepositoryVisibilitySettingPayload`](objects.md#updateenterprisememberscanchangerepositoryvisibilitysettingpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateEnterpriseMembersCanChangeRepositoryVisibilitySettingInput!`](inputs.md#updateenterprisememberscanchangerepositoryvisibilitysettinginput) |  |  |

## updateEnterpriseMembersCanCreateRepositoriesSetting

> Sets the members can create repositories setting for an enterprise.

**Type:** [`UpdateEnterpriseMembersCanCreateRepositoriesSettingPayload`](objects.md#updateenterprisememberscancreaterepositoriessettingpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateEnterpriseMembersCanCreateRepositoriesSettingInput!`](inputs.md#updateenterprisememberscancreaterepositoriessettinginput) |  |  |

## updateEnterpriseMembersCanDeleteIssuesSetting

> Sets the members can delete issues setting for an enterprise.

**Type:** [`UpdateEnterpriseMembersCanDeleteIssuesSettingPayload`](objects.md#updateenterprisememberscandeleteissuessettingpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateEnterpriseMembersCanDeleteIssuesSettingInput!`](inputs.md#updateenterprisememberscandeleteissuessettinginput) |  |  |

## updateEnterpriseMembersCanDeleteRepositoriesSetting

> Sets the members can delete repositories setting for an enterprise.

**Type:** [`UpdateEnterpriseMembersCanDeleteRepositoriesSettingPayload`](objects.md#updateenterprisememberscandeleterepositoriessettingpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateEnterpriseMembersCanDeleteRepositoriesSettingInput!`](inputs.md#updateenterprisememberscandeleterepositoriessettinginput) |  |  |

## updateEnterpriseMembersCanInviteCollaboratorsSetting

> Sets whether members can invite collaborators are enabled for an enterprise.

**Type:** [`UpdateEnterpriseMembersCanInviteCollaboratorsSettingPayload`](objects.md#updateenterprisememberscaninvitecollaboratorssettingpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateEnterpriseMembersCanInviteCollaboratorsSettingInput!`](inputs.md#updateenterprisememberscaninvitecollaboratorssettinginput) |  |  |

## updateEnterpriseMembersCanMakePurchasesSetting

> Sets whether or not an organization admin can make purchases.

**Type:** [`UpdateEnterpriseMembersCanMakePurchasesSettingPayload`](objects.md#updateenterprisememberscanmakepurchasessettingpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateEnterpriseMembersCanMakePurchasesSettingInput!`](inputs.md#updateenterprisememberscanmakepurchasessettinginput) |  |  |

## updateEnterpriseMembersCanUpdateProtectedBranchesSetting

> Sets the members can update protected branches setting for an enterprise.

**Type:** [`UpdateEnterpriseMembersCanUpdateProtectedBranchesSettingPayload`](objects.md#updateenterprisememberscanupdateprotectedbranchessettingpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateEnterpriseMembersCanUpdateProtectedBranchesSettingInput!`](inputs.md#updateenterprisememberscanupdateprotectedbranchessettinginput) |  |  |

## updateEnterpriseMembersCanViewDependencyInsightsSetting

> Sets the members can view dependency insights for an enterprise.

**Type:** [`UpdateEnterpriseMembersCanViewDependencyInsightsSettingPayload`](objects.md#updateenterprisememberscanviewdependencyinsightssettingpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateEnterpriseMembersCanViewDependencyInsightsSettingInput!`](inputs.md#updateenterprisememberscanviewdependencyinsightssettinginput) |  |  |

## updateEnterpriseOrganizationProjectsSetting

> Sets whether organization projects are enabled for an enterprise.

**Type:** [`UpdateEnterpriseOrganizationProjectsSettingPayload`](objects.md#updateenterpriseorganizationprojectssettingpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateEnterpriseOrganizationProjectsSettingInput!`](inputs.md#updateenterpriseorganizationprojectssettinginput) |  |  |

## updateEnterpriseProfile

> Updates an enterprise's profile.

**Type:** [`UpdateEnterpriseProfilePayload`](objects.md#updateenterpriseprofilepayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateEnterpriseProfileInput!`](inputs.md#updateenterpriseprofileinput) |  |  |

## updateEnterpriseRepositoryProjectsSetting

> Sets whether repository projects are enabled for a enterprise.

**Type:** [`UpdateEnterpriseRepositoryProjectsSettingPayload`](objects.md#updateenterpriserepositoryprojectssettingpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateEnterpriseRepositoryProjectsSettingInput!`](inputs.md#updateenterpriserepositoryprojectssettinginput) |  |  |

## updateEnterpriseTeamDiscussionsSetting

> Sets whether team discussions are enabled for an enterprise.

**Type:** [`UpdateEnterpriseTeamDiscussionsSettingPayload`](objects.md#updateenterpriseteamdiscussionssettingpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateEnterpriseTeamDiscussionsSettingInput!`](inputs.md#updateenterpriseteamdiscussionssettinginput) |  |  |

## updateEnterpriseTwoFactorAuthenticationRequiredSetting

> Sets whether two factor authentication is required for all users in an enterprise.

**Type:** [`UpdateEnterpriseTwoFactorAuthenticationRequiredSettingPayload`](objects.md#updateenterprisetwofactorauthenticationrequiredsettingpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateEnterpriseTwoFactorAuthenticationRequiredSettingInput!`](inputs.md#updateenterprisetwofactorauthenticationrequiredsettinginput) |  |  |

## updateIpAllowListEnabledSetting

> Sets whether an IP allow list is enabled on an owner.

**Type:** [`UpdateIpAllowListEnabledSettingPayload`](objects.md#updateipallowlistenabledsettingpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateIpAllowListEnabledSettingInput!`](inputs.md#updateipallowlistenabledsettinginput) |  |  |

## updateIpAllowListEntry

> Updates an IP allow list entry.

**Type:** [`UpdateIpAllowListEntryPayload`](objects.md#updateipallowlistentrypayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateIpAllowListEntryInput!`](inputs.md#updateipallowlistentryinput) |  |  |

## updateIssue

> Updates an Issue.

**Type:** [`UpdateIssuePayload`](objects.md#updateissuepayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateIssueInput!`](inputs.md#updateissueinput) |  |  |

## updateIssueComment

> Updates an IssueComment object.

**Type:** [`UpdateIssueCommentPayload`](objects.md#updateissuecommentpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateIssueCommentInput!`](inputs.md#updateissuecommentinput) |  |  |

## updateProject

> Updates an existing project.

**Type:** [`UpdateProjectPayload`](objects.md#updateprojectpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateProjectInput!`](inputs.md#updateprojectinput) |  |  |

## updateProjectCard

> Updates an existing project card.

**Type:** [`UpdateProjectCardPayload`](objects.md#updateprojectcardpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateProjectCardInput!`](inputs.md#updateprojectcardinput) |  |  |

## updateProjectColumn

> Updates an existing project column.

**Type:** [`UpdateProjectColumnPayload`](objects.md#updateprojectcolumnpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateProjectColumnInput!`](inputs.md#updateprojectcolumninput) |  |  |

## updatePullRequest

> Update a pull request

**Type:** [`UpdatePullRequestPayload`](objects.md#updatepullrequestpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdatePullRequestInput!`](inputs.md#updatepullrequestinput) |  |  |

## updatePullRequestReview

> Updates the body of a pull request review.

**Type:** [`UpdatePullRequestReviewPayload`](objects.md#updatepullrequestreviewpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdatePullRequestReviewInput!`](inputs.md#updatepullrequestreviewinput) |  |  |

## updatePullRequestReviewComment

> Updates a pull request review comment.

**Type:** [`UpdatePullRequestReviewCommentPayload`](objects.md#updatepullrequestreviewcommentpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdatePullRequestReviewCommentInput!`](inputs.md#updatepullrequestreviewcommentinput) |  |  |

## updateRef

> Update a Git Ref.

**Type:** [`UpdateRefPayload`](objects.md#updaterefpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateRefInput!`](inputs.md#updaterefinput) |  |  |

## updateRepository

> Update information about a repository.

**Type:** [`UpdateRepositoryPayload`](objects.md#updaterepositorypayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateRepositoryInput!`](inputs.md#updaterepositoryinput) |  |  |

## updateSubscription

> Updates the state for subscribable subjects.

**Type:** [`UpdateSubscriptionPayload`](objects.md#updatesubscriptionpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateSubscriptionInput!`](inputs.md#updatesubscriptioninput) |  |  |

## updateTeamDiscussion

> Updates a team discussion.

**Type:** [`UpdateTeamDiscussionPayload`](objects.md#updateteamdiscussionpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateTeamDiscussionInput!`](inputs.md#updateteamdiscussioninput) |  |  |

## updateTeamDiscussionComment

> Updates a discussion comment.

**Type:** [`UpdateTeamDiscussionCommentPayload`](objects.md#updateteamdiscussioncommentpayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateTeamDiscussionCommentInput!`](inputs.md#updateteamdiscussioncommentinput) |  |  |

## updateTopics

> Replaces the repository's topics with the given topics.

**Type:** [`UpdateTopicsPayload`](objects.md#updatetopicspayload)

### Arguments

| Name | Type | Description | Default Value |
| --- | --- | --- | --- |
| `input` | [`UpdateTopicsInput!`](inputs.md#updatetopicsinput) |  |  |

