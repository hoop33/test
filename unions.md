# Unions

## <a name="assignee"></a>Assignee

> Types that can be assigned to issues.

### Implemented by

* `Bot`
* `Mannequin`
* `Organization`
* `User`

## <a name="auditentryactor"></a>AuditEntryActor

> Types that can initiate an audit log event.

### Implemented by

* `Bot`
* `Organization`
* `User`

## <a name="closer"></a>Closer

> The object which triggered a `ClosedEvent`.

### Implemented by

* `Commit`
* `PullRequest`

## <a name="createdissueorrestrictedcontribution"></a>CreatedIssueOrRestrictedContribution

> Represents either a issue the viewer can access or a restricted contribution.

### Implemented by

* `CreatedIssueContribution`
* `RestrictedContribution`

## <a name="createdpullrequestorrestrictedcontribution"></a>CreatedPullRequestOrRestrictedContribution

> Represents either a pull request the viewer can access or a restricted contribution.

### Implemented by

* `CreatedPullRequestContribution`
* `RestrictedContribution`

## <a name="createdrepositoryorrestrictedcontribution"></a>CreatedRepositoryOrRestrictedContribution

> Represents either a repository the viewer can access or a restricted contribution.

### Implemented by

* `CreatedRepositoryContribution`
* `RestrictedContribution`

## <a name="enterprisemember"></a>EnterpriseMember

> An object that is a member of an enterprise.

### Implemented by

* `EnterpriseUserAccount`
* `User`

## <a name="ipallowlistowner"></a>IpAllowListOwner

> Types that can own an IP allow list.

### Implemented by

* `Enterprise`
* `Organization`

## <a name="issueorpullrequest"></a>IssueOrPullRequest

> Used for return value of Repository.issueOrPullRequest.

### Implemented by

* `Issue`
* `PullRequest`

## <a name="issuetimelineitem"></a>IssueTimelineItem

> An item in an issue timeline

### Implemented by

* `AssignedEvent`
* `ClosedEvent`
* `Commit`
* `CrossReferencedEvent`
* `DemilestonedEvent`
* `IssueComment`
* `LabeledEvent`
* `LockedEvent`
* `MilestonedEvent`
* `ReferencedEvent`
* `RenamedTitleEvent`
* `ReopenedEvent`
* `SubscribedEvent`
* `TransferredEvent`
* `UnassignedEvent`
* `UnlabeledEvent`
* `UnlockedEvent`
* `UnsubscribedEvent`
* `UserBlockedEvent`

## <a name="issuetimelineitems"></a>IssueTimelineItems

> An item in an issue timeline

### Implemented by

* `AddedToProjectEvent`
* `AssignedEvent`
* `ClosedEvent`
* `CommentDeletedEvent`
* `ConnectedEvent`
* `ConvertedNoteToIssueEvent`
* `CrossReferencedEvent`
* `DemilestonedEvent`
* `DisconnectedEvent`
* `IssueComment`
* `LabeledEvent`
* `LockedEvent`
* `MarkedAsDuplicateEvent`
* `MentionedEvent`
* `MilestonedEvent`
* `MovedColumnsInProjectEvent`
* `PinnedEvent`
* `ReferencedEvent`
* `RemovedFromProjectEvent`
* `RenamedTitleEvent`
* `ReopenedEvent`
* `SubscribedEvent`
* `TransferredEvent`
* `UnassignedEvent`
* `UnlabeledEvent`
* `UnlockedEvent`
* `UnmarkedAsDuplicateEvent`
* `UnpinnedEvent`
* `UnsubscribedEvent`
* `UserBlockedEvent`

## <a name="milestoneitem"></a>MilestoneItem

> Types that can be inside a Milestone.

### Implemented by

* `Issue`
* `PullRequest`

## <a name="orgrestorememberauditentrymembership"></a>OrgRestoreMemberAuditEntryMembership

> Types of memberships that can be restored for an Organization member.

### Implemented by

* `OrgRestoreMemberMembershipOrganizationAuditEntryData`
* `OrgRestoreMemberMembershipRepositoryAuditEntryData`
* `OrgRestoreMemberMembershipTeamAuditEntryData`

## <a name="organizationauditentry"></a>OrganizationAuditEntry

> An audit entry in an organization audit log.

### Implemented by

* `MembersCanDeleteReposClearAuditEntry`
* `MembersCanDeleteReposDisableAuditEntry`
* `MembersCanDeleteReposEnableAuditEntry`
* `OauthApplicationCreateAuditEntry`
* `OrgAddBillingManagerAuditEntry`
* `OrgAddMemberAuditEntry`
* `OrgBlockUserAuditEntry`
* `OrgConfigDisableCollaboratorsOnlyAuditEntry`
* `OrgConfigEnableCollaboratorsOnlyAuditEntry`
* `OrgCreateAuditEntry`
* `OrgDisableOauthAppRestrictionsAuditEntry`
* `OrgDisableSamlAuditEntry`
* `OrgDisableTwoFactorRequirementAuditEntry`
* `OrgEnableOauthAppRestrictionsAuditEntry`
* `OrgEnableSamlAuditEntry`
* `OrgEnableTwoFactorRequirementAuditEntry`
* `OrgInviteMemberAuditEntry`
* `OrgInviteToBusinessAuditEntry`
* `OrgOauthAppAccessApprovedAuditEntry`
* `OrgOauthAppAccessDeniedAuditEntry`
* `OrgOauthAppAccessRequestedAuditEntry`
* `OrgRemoveBillingManagerAuditEntry`
* `OrgRemoveMemberAuditEntry`
* `OrgRemoveOutsideCollaboratorAuditEntry`
* `OrgRestoreMemberAuditEntry`
* `OrgUnblockUserAuditEntry`
* `OrgUpdateDefaultRepositoryPermissionAuditEntry`
* `OrgUpdateMemberAuditEntry`
* `OrgUpdateMemberRepositoryCreationPermissionAuditEntry`
* `OrgUpdateMemberRepositoryInvitationPermissionAuditEntry`
* `PrivateRepositoryForkingDisableAuditEntry`
* `PrivateRepositoryForkingEnableAuditEntry`
* `RepoAccessAuditEntry`
* `RepoAddMemberAuditEntry`
* `RepoAddTopicAuditEntry`
* `RepoArchivedAuditEntry`
* `RepoChangeMergeSettingAuditEntry`
* `RepoConfigDisableAnonymousGitAccessAuditEntry`
* `RepoConfigDisableCollaboratorsOnlyAuditEntry`
* `RepoConfigDisableContributorsOnlyAuditEntry`
* `RepoConfigDisableSockpuppetDisallowedAuditEntry`
* `RepoConfigEnableAnonymousGitAccessAuditEntry`
* `RepoConfigEnableCollaboratorsOnlyAuditEntry`
* `RepoConfigEnableContributorsOnlyAuditEntry`
* `RepoConfigEnableSockpuppetDisallowedAuditEntry`
* `RepoConfigLockAnonymousGitAccessAuditEntry`
* `RepoConfigUnlockAnonymousGitAccessAuditEntry`
* `RepoCreateAuditEntry`
* `RepoDestroyAuditEntry`
* `RepoRemoveMemberAuditEntry`
* `RepoRemoveTopicAuditEntry`
* `RepositoryVisibilityChangeDisableAuditEntry`
* `RepositoryVisibilityChangeEnableAuditEntry`
* `TeamAddMemberAuditEntry`
* `TeamAddRepositoryAuditEntry`
* `TeamChangeParentTeamAuditEntry`
* `TeamRemoveMemberAuditEntry`
* `TeamRemoveRepositoryAuditEntry`

## <a name="permissiongranter"></a>PermissionGranter

> Types that can grant permissions on a repository to a user

### Implemented by

* `Organization`
* `Repository`
* `Team`

## <a name="pinnableitem"></a>PinnableItem

> Types that can be pinned to a profile page.

### Implemented by

* `Gist`
* `Repository`

## <a name="projectcarditem"></a>ProjectCardItem

> Types that can be inside Project Cards.

### Implemented by

* `Issue`
* `PullRequest`

## <a name="pullrequesttimelineitem"></a>PullRequestTimelineItem

> An item in an pull request timeline

### Implemented by

* `AssignedEvent`
* `BaseRefForcePushedEvent`
* `ClosedEvent`
* `CommitCommentThread`
* `Commit`
* `CrossReferencedEvent`
* `DemilestonedEvent`
* `DeployedEvent`
* `DeploymentEnvironmentChangedEvent`
* `HeadRefDeletedEvent`
* `HeadRefForcePushedEvent`
* `HeadRefRestoredEvent`
* `IssueComment`
* `LabeledEvent`
* `LockedEvent`
* `MergedEvent`
* `MilestonedEvent`
* `PullRequestReviewComment`
* `PullRequestReviewThread`
* `PullRequestReview`
* `ReferencedEvent`
* `RenamedTitleEvent`
* `ReopenedEvent`
* `ReviewDismissedEvent`
* `ReviewRequestRemovedEvent`
* `ReviewRequestedEvent`
* `SubscribedEvent`
* `UnassignedEvent`
* `UnlabeledEvent`
* `UnlockedEvent`
* `UnsubscribedEvent`
* `UserBlockedEvent`

## <a name="pullrequesttimelineitems"></a>PullRequestTimelineItems

> An item in a pull request timeline

### Implemented by

* `AddedToProjectEvent`
* `AssignedEvent`
* `AutomaticBaseChangeFailedEvent`
* `AutomaticBaseChangeSucceededEvent`
* `BaseRefChangedEvent`
* `BaseRefForcePushedEvent`
* `ClosedEvent`
* `CommentDeletedEvent`
* `ConnectedEvent`
* `ConvertToDraftEvent`
* `ConvertedNoteToIssueEvent`
* `CrossReferencedEvent`
* `DemilestonedEvent`
* `DeployedEvent`
* `DeploymentEnvironmentChangedEvent`
* `DisconnectedEvent`
* `HeadRefDeletedEvent`
* `HeadRefForcePushedEvent`
* `HeadRefRestoredEvent`
* `IssueComment`
* `LabeledEvent`
* `LockedEvent`
* `MarkedAsDuplicateEvent`
* `MentionedEvent`
* `MergedEvent`
* `MilestonedEvent`
* `MovedColumnsInProjectEvent`
* `PinnedEvent`
* `PullRequestCommitCommentThread`
* `PullRequestCommit`
* `PullRequestReviewThread`
* `PullRequestReview`
* `PullRequestRevisionMarker`
* `ReadyForReviewEvent`
* `ReferencedEvent`
* `RemovedFromProjectEvent`
* `RenamedTitleEvent`
* `ReopenedEvent`
* `ReviewDismissedEvent`
* `ReviewRequestRemovedEvent`
* `ReviewRequestedEvent`
* `SubscribedEvent`
* `TransferredEvent`
* `UnassignedEvent`
* `UnlabeledEvent`
* `UnlockedEvent`
* `UnmarkedAsDuplicateEvent`
* `UnpinnedEvent`
* `UnsubscribedEvent`
* `UserBlockedEvent`

## <a name="pushallowanceactor"></a>PushAllowanceActor

> Types that can be an actor.

### Implemented by

* `App`
* `Team`
* `User`

## <a name="referencedsubject"></a>ReferencedSubject

> Any referencable object

### Implemented by

* `Issue`
* `PullRequest`

## <a name="renamedtitlesubject"></a>RenamedTitleSubject

> An object which has a renamable title

### Implemented by

* `Issue`
* `PullRequest`

## <a name="requestedreviewer"></a>RequestedReviewer

> Types that can be requested reviewers.

### Implemented by

* `Mannequin`
* `Team`
* `User`

## <a name="reviewdismissalallowanceactor"></a>ReviewDismissalAllowanceActor

> Types that can be an actor.

### Implemented by

* `Team`
* `User`

## <a name="searchresultitem"></a>SearchResultItem

> The results of a search.

### Implemented by

* `App`
* `Issue`
* `MarketplaceListing`
* `Organization`
* `PullRequest`
* `Repository`
* `User`

## <a name="sponsor"></a>Sponsor

> Entites that can sponsor others via GitHub Sponsors

### Implemented by

* `Organization`
* `User`

## <a name="statuscheckrollupcontext"></a>StatusCheckRollupContext

> Types that can be inside a StatusCheckRollup context.

### Implemented by

* `StatusContext`

